package com.travel.controller.standingBook;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.StringUtils;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.DdStudentStanding;
import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzPoorStudentStanding;
import com.travel.system.domain.TzSpecialStudentStanding;
import com.travel.system.service.IDdStudentStandingService;
import com.travel.system.service.IEduStudentService;
import com.travel.system.service.ITzPoorStudentStandingService;
import com.travel.system.service.ITzSpecialStudentStandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 未入学儿童台账Controller
 *
 * @author xianyue
 * @date 2021-01-25
 */
@RestController
@RequestMapping("/normal/standingBook")
public class StandingBookController extends BaseController {
    @Autowired
    private IDdStudentStandingService ddStudentStandingService;
    @Autowired
    private IEduStudentService eduStudentService;
    @Autowired
    private ITzPoorStudentStandingService tzPoorStudentStandingService;
    @Autowired
    private ITzSpecialStudentStandingService tzSpecialStudentStandingService;

    /**
     * 查询未入学儿童台账列表
     */
//    @PreAuthorize("@ss.hasPermi('')")
    @GetMapping("/list")
    public TableDataInfo list(DdStudentStanding ddStudentStanding) {
        startPage();
        List<DdStudentStanding> list = ddStudentStandingService.selectDdStudentStandingList(ddStudentStanding);
        return getDataTable(list);
    }

    /**
     * 导出未入学儿童台账列表
     */
//    @PreAuthorize("@ss.hasPermi('')")
    @Log(title = "未入学儿童台账", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdStudentStanding ddStudentStanding) {
        List<DdStudentStanding> list = ddStudentStandingService.selectDdStudentStandingList(ddStudentStanding);
        ExcelUtil<DdStudentStanding> util = new ExcelUtil<DdStudentStanding>(DdStudentStanding.class);
        return util.exportExcel(list, "standing");
    }
//    /**
//     * 获取未入学儿童台账详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:standing:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Integer id) {
//        return AjaxResult.success(ddStudentStandingService.selectDdStudentStandingById(id));
//    }
//    /**
//     * 新增未入学儿童台账
//     */
//    @PreAuthorize("@ss.hasPermi('system:standing:add')")
//    @Log(title = "未入学儿童台账", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody DdStudentStanding ddStudentStanding) {
//        return toAjax(ddStudentStandingService.insertDdStudentStanding(ddStudentStanding));
//    }
//
//    /**
//     * 修改未入学儿童台账
//     */
//    @PreAuthorize("@ss.hasPermi('system:standing:edit')")
//    @Log(title = "未入学儿童台账", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody DdStudentStanding ddStudentStanding) {
//        return toAjax(ddStudentStandingService.updateDdStudentStanding(ddStudentStanding));
//    }
//
//    /**
//     * 删除未入学儿童台账
//     */
//    @PreAuthorize("@ss.hasPermi('system:standing:remove')")
//    @Log(title = "未入学儿童台账", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Integer[] ids) {
//        return toAjax(ddStudentStandingService.deleteDdStudentStandingByIds(ids));
//    }

    /**
     * 获取统计年份
     * 1未入学儿童台账年份获取
     * 2贫困学生台账
     * 3.特殊儿童台账
     */
    @GetMapping("/getTotalYear/{type}")
    public AjaxResult getTotalYear(@PathVariable("type") Integer type) {
        List<?> list = new ArrayList<>();
        if (type == 1) {
            list = ddStudentStandingService.getTotalYear();
//            return AjaxResult.success(list);
        } else if (type == 2) {
            list = tzPoorStudentStandingService.getTotalYear();
        } else if (type == 3) {
            list = tzSpecialStudentStandingService.getTotalYear();
        }
        return AjaxResult.success(list);
    }

    /**
     * 生成未入学儿童台账
     */
    @GetMapping("/createStudentStanding")
    public AjaxResult createStudentStanding() {
        //获取当前年份
        Calendar date = Calendar.getInstance();
        Integer year = Integer.valueOf(date.get(Calendar.YEAR));
        String year6_s = String.valueOf(year - 6) + "-08-31 00:00:00";
        String year16_s = String.valueOf(year - 17) + "-08-31 23:59:59";
        //把字符串日期转变为日期类数据
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        List<EduStudent> list = new ArrayList<>();
        AjaxResult res;
        try {
            Date year6 = format.parse(year6_s);
            Date year16 = format.parse(year16_s);
            //根据日期获取满足6-16周岁的学生
            list = eduStudentService.getStudentByDate(year6, year16);
            if (StringUtils.isEmpty(list)) {
                res = AjaxResult.error("暂无符合条件学生！");
            } else {
                ddStudentStandingService.insertStudentToStanding(list);
                res = AjaxResult.success();
            }
        } catch (ParseException e) {
            res = AjaxResult.error();
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 获取贫困儿童台账列表
     */
//    @PreAuthorize("@ss.hasPermi('')")
    @GetMapping("/poorStudentList")
    public TableDataInfo list(TzPoorStudentStanding tzPoorStudentStanding) {
        startPage();
        List<TzPoorStudentStanding> list = tzPoorStudentStandingService.selectTzPoorStudentStandingList(tzPoorStudentStanding);
        return getDataTable(list);
    }

    /**
     * 生成贫困儿童台账
     */
    @GetMapping("/createPoorStudentStanding")
    public AjaxResult createPoorStudentStanding() {
        //获取当前年份
        Calendar date = Calendar.getInstance();
        Integer year = Integer.valueOf(date.get(Calendar.YEAR));
        String year6_s = String.valueOf(year - 6) + "-08-31 00:00:00";
        String year16_s = String.valueOf(year - 17) + "-08-31 23:59:59";
        //把字符串日期转变为日期类数据
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        List<EduStudent> list = new ArrayList<>();
        AjaxResult res;
        try {
            Date year6 = format.parse(year6_s);
            Date year16 = format.parse(year16_s);
            //根据日期获取满足6-16周岁的学生
            list = eduStudentService.getPoorStudent(year6, year16);
            if (StringUtils.isEmpty(list)) {
                res = AjaxResult.error("暂无符合条件学生！");
            } else {
                tzPoorStudentStandingService.insertStudent(list);
                res = AjaxResult.success();
            }
        } catch (ParseException e) {
            res = AjaxResult.error();
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 获取贫困学生台账列表
     */
//    @PreAuthorize("@ss.hasPermi('system:standing:list')")
    @GetMapping("/specialStudentList")
    public TableDataInfo list(TzSpecialStudentStanding tzSpecialStudentStanding) {
        startPage();
        List<TzSpecialStudentStanding> list = tzSpecialStudentStandingService.selectTzSpecialStudentStandingList(tzSpecialStudentStanding);
        return getDataTable(list);
    }

    /**
     * 生成特殊儿童台账
     */
    @GetMapping("/createSpecialStudentStanding")
    public AjaxResult createSpecialStudentStanding() {
        //获取当前年份
        Calendar date = Calendar.getInstance();
        Integer year = Integer.valueOf(date.get(Calendar.YEAR));
        String year6_s = String.valueOf(year - 6) + "-08-31 00:00:00";
        String year16_s = String.valueOf(year - 17) + "-08-31 23:59:59";
        //把字符串日期转变为日期类数据
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        List<EduStudent> list = new ArrayList<>();
        AjaxResult res;
        try {
            Date year6 = format.parse(year6_s);
            Date year16 = format.parse(year16_s);
            //根据日期获取满足6-16周岁的学生
            list = eduStudentService.getPoorStudent(year6, year16);
            if (StringUtils.isEmpty(list)) {
                res = AjaxResult.error("暂无符合条件学生！");
            } else {
                tzSpecialStudentStandingService.insertSpecialStudent(list);
                res = AjaxResult.success();
            }
        } catch (ParseException e) {
            res = AjaxResult.error();
            e.printStackTrace();
        }
        return res;
    }

}
