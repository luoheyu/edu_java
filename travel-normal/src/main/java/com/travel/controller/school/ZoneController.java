package com.travel.controller.school;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.EduSchool;
import com.travel.system.domain.EduZone;
import com.travel.system.service.IEduSchoolService;
import com.travel.system.service.IEduZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 村镇管理Controller
 *
 * @author yu
 * @date 2020-12-23
 */

@RestController
@RequestMapping("/normal/zone")
public class ZoneController extends BaseController {
    @Autowired
    private IEduZoneService eduZoneService;
    @Autowired
    private IEduSchoolService eduSchoolService;

    /**
     * 查询村镇管理列表
     */
    @PreAuthorize("@ss.hasPermi('normal:zone:list')")
    @GetMapping("/list")
    public AjaxResult list(EduZone eduZone) {
        List<EduZone> list = eduZoneService.selectEduZoneList(eduZone);
        return AjaxResult.success(list);
    }


    /**
     * 导出村镇管理列表
     */
    @PreAuthorize("@ss.hasPermi('normal:zone:export')")
    @Log(title = "村镇管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EduZone eduZone) {
        List<EduZone> list = eduZoneService.selectEduZoneList(eduZone);
        ExcelUtil<EduZone> util = new ExcelUtil<EduZone>(EduZone.class);
        return util.exportExcel(list, "zone");
    }


    /**
     * 获取村镇管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('normal:zone:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(eduZoneService.selectEduZoneById(id));
    }


    /**
     * 新增村镇管理
     */
    @PreAuthorize("@ss.hasPermi('normal:zone:add')")
    @Log(title = "村镇管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EduZone eduZone) {
        return toAjax(eduZoneService.insertEduZone(eduZone));
    }


    /**
     * 修改村镇管理
     */
    @PreAuthorize("@ss.hasPermi('normal:zone:edit')")
    @Log(title = "村镇管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EduZone eduZone) {
        return toAjax(eduZoneService.updateEduZone(eduZone));
    }


    /**
     * 删除村镇管理
     */
    @PreAuthorize("@ss.hasPermi('normal:zone:remove')")
    @Log(title = "村镇管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(eduZoneService.deleteEduZoneByIds(ids));

    }
    /**
     * 获取镇列表
     */
    @GetMapping("/getZhen")
    public AjaxResult getZhen(){
        List<EduZone> zhen = eduZoneService.getZhen();
        return AjaxResult.success(zhen);
    }
    /**
     * 根据fid获取村镇列表
     */
    @GetMapping("/getZoneList/{fid}")
    public AjaxResult getZoneList(@PathVariable("fid") Integer fid){
        List<EduZone> list = eduZoneService.getZoneList(fid);
        return AjaxResult.success(list);
    }
    /**
     * 根据id获取村镇名称
     */
    @GetMapping("/getNameById/{id}")
    public AjaxResult getNameById(@PathVariable("id") Integer id){
        EduZone eduZone = eduZoneService.getNameById(id);
        return AjaxResult.success(eduZone);
    }
    /**
     * 根据id获取学校名称
     */
    @GetMapping("/getSchoolNameById/{id}")
    public AjaxResult getSchoolNameById(@PathVariable("id") Integer id){
        EduSchool eduSchool = eduSchoolService.getSchoolNameId(id);
        return AjaxResult.success(eduSchool);
    }
    /**
     * 根据fid获取所有初中
     */
    @GetMapping("/getSchoolList/{fid}")
    public AjaxResult getSchoolList(@PathVariable("fid") Integer fid){
        List<EduSchool> schoolList = eduSchoolService.getSchoolList(fid);
        return AjaxResult.success(schoolList);
    }
    /**
    *获取所有村镇列表
    *
    */
    @GetMapping("/getAllZone")
    public AjaxResult getAllZone(){
        List<EduZone> list = eduZoneService.getAllZone();
        return  AjaxResult.success(list);
    }
}
