package com.travel.controller.school;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.DataModel.SchoolTreeSelect;
import com.travel.system.domain.EduSchool;
import com.travel.system.service.IEduSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/normal/school")
public class SchoolController extends BaseController {
    @Autowired
    private IEduSchoolService eduSchoolService;


    /**
     * 查询创建列表
     */

    @PreAuthorize("@ss.hasPermi('normal:school:list')")
    @GetMapping("/list")
    public AjaxResult list(EduSchool eduSchool) {
        startPage();
        List<EduSchool> list = eduSchoolService.selectEduSchoolList(eduSchool);
        return AjaxResult.success(list);
    }


    /**
     * 导出创建列表
     */

    @PreAuthorize("@ss.hasPermi('normal:school:export')")
    @Log(title = "创建", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EduSchool eduSchool) {
        List<EduSchool> list = eduSchoolService.selectEduSchoolList(eduSchool);
        ExcelUtil<EduSchool> util = new ExcelUtil<EduSchool>(EduSchool.class);
        return util.exportExcel(list, "school");
    }


    /**
     * 获取创建详细信息
     */

    @PreAuthorize("@ss.hasPermi('normal:school:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(eduSchoolService.selectEduSchoolById(id));
    }


    /**
     * 新增创建
     */

    @PreAuthorize("@ss.hasPermi('normal:school:add')")
    @Log(title = "创建", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EduSchool eduSchool) {
        return toAjax(eduSchoolService.insertEduSchool(eduSchool));
    }


    /**
     * 修改创建
     */

    @PreAuthorize("@ss.hasPermi('normal:school:edit')")
    @Log(title = "创建", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EduSchool eduSchool) {
        return toAjax(eduSchoolService.updateEduSchool(eduSchool));
    }


    /**
     * 删除创建
     */
    @PreAuthorize("@ss.hasPermi('normal:school:remove')")
    @Log(title = "创建", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(eduSchoolService.deleteEduSchoolByIds(ids));
    }

    /**
     * 获取中学下拉下拉树列表
     */
    @GetMapping("/getMiddle")
    public AjaxResult getMiddle() {
        List<EduSchool> middles = eduSchoolService.getAllMiddle();
        return AjaxResult.success(middles);
    }

    /**
     * 根据fid获取学校列表
     */
    @GetMapping("/getSchoolList/{fid}")
    public AjaxResult getSchoolList(@PathVariable("fid") Integer fid) {
        List<EduSchool> list = eduSchoolService.getSchoolList(fid);
        return AjaxResult.success(list);
    }

    /**
     * 根据小学id找到父级id
     */
    @GetMapping("/getSchoolParent/{id}")
    public AjaxResult getSchoolParent(@PathVariable("id") Integer id) {
        EduSchool eduSchool = eduSchoolService.getSchoolParent(id);
        return AjaxResult.success(eduSchool);
    }

    /**
     * 获取所有学校数据
     */
    @GetMapping("/getAllSchool")
    public AjaxResult getAllSchool() {
        List<EduSchool> allSchool = eduSchoolService.getAllSchool();
        return AjaxResult.success(allSchool);
    }

    /**
     * 根据镇id获取下属学校
     */
    @GetMapping("/getSchoolByTownId/{id}")
    public AjaxResult getSchoolByTownId(@PathVariable Integer id) {
        List<EduSchool> list = eduSchoolService.getSchoolByTownId(id);
        return AjaxResult.success(list);
    }

    /**
     * 获取学校下拉树列表
     */
    @GetMapping("/schoolTreeSelect")
    public AjaxResult schoolTreeSelect(EduSchool eduSchool) {
        List<EduSchool> school = eduSchoolService.selectEduSchoolList(eduSchool);
        List<SchoolTreeSelect> list = eduSchoolService.buildSchoolTreeSelect(school);
        return AjaxResult.success(list);
    }

}
