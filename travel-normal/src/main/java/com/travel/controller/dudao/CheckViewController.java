package com.travel.controller.dudao;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.IDdAssTempService;
import com.travel.system.domain.DdAssTemp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模板考核，各个学校考核记录Controller
 *
 * @author xianyue
 * @date 2021-01-14
 */
@RestController
@RequestMapping("/normal/dudao/checkView")
public class CheckViewController extends BaseController {
    @Autowired
    private IDdAssTempService ddAssTempService;
    /**
     * 查询模板考核，各个学校考核记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:temp:list')")
    @GetMapping("/list")
    public TableDataInfo list(DdAssTemp ddAssTemp) {
        startPage();
        List<DdAssTemp> list = ddAssTempService.selectDdAssTempList(ddAssTemp);
        return getDataTable(list);
    }
    /**
     * 导出模板考核，各个学校考核记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:temp:export')")
    @Log(title = "模板考核，各个学校考核记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdAssTemp ddAssTemp) {
        List<DdAssTemp> list = ddAssTempService.selectDdAssTempList(ddAssTemp);
        ExcelUtil<DdAssTemp> util = new ExcelUtil<DdAssTemp>(DdAssTemp.class);
        return util.exportExcel(list, "temp");
    }
    /**
     * 获取模板考核，各个学校考核记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:temp:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(ddAssTempService.selectDdAssTempById(id));
    }
    /**
     * 新增模板考核，各个学校考核记录
     */
    @PreAuthorize("@ss.hasPermi('system:temp:add')")
    @Log(title = "模板考核，各个学校考核记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdAssTemp ddAssTemp) {
        return toAjax(ddAssTempService.insertDdAssTemp(ddAssTemp));
    }
    /**
     * 修改模板考核，各个学校考核记录
     */
    @PreAuthorize("@ss.hasPermi('system:temp:edit')")
    @Log(title = "模板考核，各个学校考核记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdAssTemp ddAssTemp) {
        return toAjax(ddAssTempService.updateDdAssTemp(ddAssTemp));
    }
    /**
     * 删除模板考核，各个学校考核记录
     */
    @PreAuthorize("@ss.hasPermi('system:temp:remove')")
    @Log(title = "模板考核，各个学校考核记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(ddAssTempService.deleteDdAssTempByIds(ids));
    }
}
