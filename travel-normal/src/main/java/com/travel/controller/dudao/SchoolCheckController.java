package com.travel.controller.dudao;

import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.domain.entity.SysUser;
import com.travel.common.core.domain.model.LoginUser;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.utils.ServletUtils;
import com.travel.common.utils.StringUtils;
import com.travel.framework.web.service.SysPermissionService;
import com.travel.framework.web.service.TokenService;
import com.travel.system.IDdAssTempService;
import com.travel.system.domain.DdAssTemp;
import com.travel.system.domain.DdAssTempItem;
import com.travel.system.domain.DdAssess;
import com.travel.system.domain.DdAssessitem;
import com.travel.system.service.IDdAssTempItemService;
import com.travel.system.service.IDdAssessService;
import com.travel.system.service.IDdAssessitemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/dudao/schoolCheck")
public class SchoolCheckController extends BaseController {
    @Autowired
    private IDdAssessService ddAssessService;
    @Autowired
    private IDdAssessitemService ddAssessitemService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private SysPermissionService permissionService;
    @Autowired
    private IDdAssTempService ddAssTempService;
    @Autowired
    private IDdAssTempItemService ddAssTempItemService;

    /**
     * 获取数据列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DdAssTemp ddAssTemp) {
        startPage();
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        //以后改为根据用户的schoolid来赋值ddasstemp的schoolid
        ddAssTemp.setSchoolId(1);
        List<DdAssTemp> list = ddAssTempService.selectDdAssTempList(ddAssTemp);
        return getDataTable(list);
    }

    /**
     * 获取考核项目的条目
     */
    @GetMapping("/getItemList")
    public TableDataInfo getItemList(DdAssTempItem ddAssTempItem) {
        startPage();
        List<DdAssTempItem> list = ddAssTempItemService.selectDdAssTempItemList(ddAssTempItem);
        return getDataTable(list);
    }
    /**
    *考核反馈
    *
    */
    @GetMapping("/feedBackDetail/{id}")
    public AjaxResult feedBackDetail(@PathVariable("id") Integer id){
        DdAssTempItem detail = ddAssTempItemService.getDetailInfo(id);
        AjaxResult ajax = AjaxResult.success(detail);
        return ajax;
    }
    /**
    *更新反馈数据
    *
    */
    @PutMapping
    public AjaxResult update(@RequestBody DdAssTempItem ddAssTempItem){
        return  toAjax(ddAssTempItemService.updateDdAssTempItem(ddAssTempItem));
    }
    /**
    *获取补充内容列表
    *
    */
    @GetMapping("/supplementLIst/{schoolId}/{tempId}")
    public TableDataInfo supplementList(DdAssessitem ddAssessitem,@PathVariable("schoolId") Integer schoolId,@PathVariable("tempId") Integer tempId){
        startPage();
        DdAssess assess = ddAssessService.getByIds(schoolId,tempId);
        List<DdAssessitem> list = new ArrayList<>();
        if(StringUtils.isNotNull(assess)){
            ddAssessitem.setAssId(assess.getId());
            list = ddAssessitemService.selectDdAssessitemList(ddAssessitem);
        }
        return getDataTable(list);
    }
    /**
     *根据id获取详细内容
     *
     */
    @GetMapping("/supplementDetail/{id}")
    public AjaxResult supplementDetail(@PathVariable("id") Integer id){
        DdAssessitem d = ddAssessitemService.getDetail(id);
        return AjaxResult.success(d);
    }
    /**
    *补充内容反馈更新操作
    *
    */
    @PutMapping("/feedbackUpdate")
    public AjaxResult feedbackUpdate(@RequestBody DdAssessitem ddAssessitem){
        return toAjax(ddAssessitemService.updateDdAssessitem(ddAssessitem));
    }
}
