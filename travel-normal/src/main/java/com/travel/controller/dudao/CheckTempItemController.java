package com.travel.controller.dudao;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.DdAssTempItem;
import com.travel.system.service.IDdAssTempItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模板考核，各个学校考核记录之单个条目记录Controller
 *
 * @author xianyue
 * @date 2021-01-14
 */
@RestController
@RequestMapping("/normal/dudao/checkTempItem")
public class CheckTempItemController extends BaseController {
    @Autowired
    private IDdAssTempItemService ddAssTempItemService;

    /**
     * 查询模板考核，各个学校考核记录之单个条目记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(DdAssTempItem ddAssTempItem) {
        startPage();
        List<DdAssTempItem> list = ddAssTempItemService.selectDdAssTempItemList(ddAssTempItem);
        return getDataTable(list);
    }

    /**
     * 导出模板考核，各个学校考核记录之单个条目记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:item:export')")
    @Log(title = "模板考核，各个学校考核记录之单个条目记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdAssTempItem ddAssTempItem) {
        List<DdAssTempItem> list = ddAssTempItemService.selectDdAssTempItemList(ddAssTempItem);
        ExcelUtil<DdAssTempItem> util = new ExcelUtil<DdAssTempItem>(DdAssTempItem.class);
        return util.exportExcel(list, "item");
    }

    /**
     * 获取模板考核，各个学校考核记录之单个条目记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(ddAssTempItemService.selectDdAssTempItemById(id));
    }

    /**
     * 新增模板考核，各个学校考核记录之单个条目记录
     */
    @PreAuthorize("@ss.hasPermi('system:item:add')")
    @Log(title = "模板考核，各个学校考核记录之单个条目记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdAssTempItem ddAssTempItem) {
        return toAjax(ddAssTempItemService.insertDdAssTempItem(ddAssTempItem));
    }

    /**
     * 修改模板考核，各个学校考核记录之单个条目记录
     */
    @PreAuthorize("@ss.hasPermi('system:item:edit')")
    @Log(title = "模板考核，各个学校考核记录之单个条目记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdAssTempItem ddAssTempItem) {
        return toAjax(ddAssTempItemService.updateDdAssTempItem(ddAssTempItem));
    }

    /**
     * 删除模板考核，各个学校考核记录之单个条目记录
     */
    @PreAuthorize("@ss.hasPermi('system:item:remove')")
    @Log(title = "模板考核，各个学校考核记录之单个条目记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(ddAssTempItemService.deleteDdAssTempItemByIds(ids));
    }
    /**
    *根据id获取详情
    *
    */
    @GetMapping("/getDetail/{id}")
    public AjaxResult getDetail(@PathVariable Integer id){
        DdAssTempItem detail = ddAssTempItemService.getDetailInfo(id);
        return AjaxResult.success(detail);
    }
}
