package com.travel.controller.dudao;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.StringUtils;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.DdAssess;
import com.travel.system.domain.DdAssessitem;
import com.travel.system.service.IDdAssessService;
import com.travel.system.service.IDdAssessitemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 补充内容表Controller
 *
 * @author xianyue
 * @date 2021-01-18
 */
@RestController
@RequestMapping("/normal/supplement")
public class SupplementController extends BaseController {
    @Autowired
    private IDdAssessService ddAssessService;
    @Autowired
    private IDdAssessitemService ddAssessitemService;

    /**
     * 查询反馈内容细项列表
     */
    @PreAuthorize("@ss.hasPermi('normal:supplement:list')")
    @GetMapping("/list/{schoolId}/{tempId}")
    public TableDataInfo list(DdAssessitem ddAssessitem,@PathVariable("schoolId") Integer schoolId,@PathVariable("tempId") Integer tempId) {
        startPage();
        DdAssess assess = ddAssessService.getByIds(schoolId,tempId);
        List<DdAssessitem> list = new ArrayList<>();
        if(StringUtils.isNotNull(assess)){
            ddAssessitem.setAssId(assess.getId());
            list = ddAssessitemService.selectDdAssessitemList(ddAssessitem);
        }
        return getDataTable(list);
    }

    /**
     * 导出反馈内容细项列表
     */
    @PreAuthorize("@ss.hasPermi('normal:supplement:export')")
    @Log(title = "反馈内容细项", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdAssessitem ddAssessitem) {
        List<DdAssessitem> list = ddAssessitemService.selectDdAssessitemList(ddAssessitem);
        ExcelUtil<DdAssessitem> util = new ExcelUtil<DdAssessitem>(DdAssessitem.class);
        return util.exportExcel(list, "assessitem");
    }

    /**
     * 获取反馈内容细项详细信息
     */
    @PreAuthorize("@ss.hasPermi('normal:supplement:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(ddAssessitemService.selectDdAssessitemById(id));
    }

    /**
     * 新增反馈内容细项
     */
    @PreAuthorize("@ss.hasPermi('normal:supplement:add')")
    @Log(title = "反馈内容细项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdAssessitem ddAssessitem) {
        return toAjax(ddAssessitemService.insertDdAssessitem(ddAssessitem));
    }

    /**
     * 修改反馈内容细项
     */
    @PreAuthorize("@ss.hasPermi('normal:supplement:edit')")
    @Log(title = "反馈内容细项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdAssessitem ddAssessitem) {
        return toAjax(ddAssessitemService.updateDdAssessitem(ddAssessitem));
    }

    /**
     * 删除反馈内容细项
     */
    @PreAuthorize("@ss.hasPermi('normal:supplement:remove')")
    @Log(title = "反馈内容细项", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(ddAssessitemService.deleteDdAssessitemByIds(ids));
    }
    /**
    *根据id获取详细内容
    *
    */
    @GetMapping("getDetail/{id}")
    public AjaxResult getDetail(@PathVariable("id") Integer id){
        DdAssessitem d = ddAssessitemService.getDetail(id);
        return AjaxResult.success(d);
    }
}
