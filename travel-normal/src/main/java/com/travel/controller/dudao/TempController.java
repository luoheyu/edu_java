package com.travel.controller.dudao;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.DdTempName;
import com.travel.system.service.IDdTempNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模板名（01）Controller
 *
 * @author xianyue
 * @date 2021-01-11
 */
@RestController
@RequestMapping("/normal/dudao/temp")
public class TempController extends BaseController {
    @Autowired
    private IDdTempNameService ddTempNameService;

    /**
     * 查询模板名（01）列表
     */
    @PreAuthorize("@ss.hasPermi('system:name:list')")
    @GetMapping("/list")
    public TableDataInfo list(DdTempName ddTempName) {
        startPage();
        List<DdTempName> list = ddTempNameService.selectDdTempNameList(ddTempName);
        return getDataTable(list);
    }

    /**
     * 导出模板名（01）列表
     */
    @PreAuthorize("@ss.hasPermi('system:name:export')")
    @Log(title = "模板名（01）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdTempName ddTempName) {
        List<DdTempName> list = ddTempNameService.selectDdTempNameList(ddTempName);
        ExcelUtil<DdTempName> util = new ExcelUtil<DdTempName>(DdTempName.class);
        return util.exportExcel(list, "name");
    }

    /**
     * 获取模板名（01）详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:name:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(ddTempNameService.selectDdTempNameById(id));
    }

    /**
     * 新增模板名（01）
     */
    @PreAuthorize("@ss.hasPermi('system:name:add')")
    @Log(title = "模板名（01）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdTempName ddTempName) {
        return toAjax(ddTempNameService.insertDdTempName(ddTempName));
    }

    /**
     * 修改模板名（01）
     */
    @PreAuthorize("@ss.hasPermi('system:name:edit')")
    @Log(title = "模板名（01）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdTempName ddTempName) {
        return toAjax(ddTempNameService.updateDdTempName(ddTempName));
    }

    /**
     * 删除模板名（01）
     */
    @PreAuthorize("@ss.hasPermi('system:name:remove')")
    @Log(title = "模板名（01）", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(ddTempNameService.deleteDdTempNameByIds(ids));
    }
    /**
    *获取所有模板的列表
    *
    */
    @GetMapping("/getTempList")
    public AjaxResult getTempList(){
        List<DdTempName> list = ddTempNameService.getTempList();
        return AjaxResult.success(list);
    }
}
