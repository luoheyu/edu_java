package com.travel.controller.dudao;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.DdTempItem;
import com.travel.system.service.IDdTempItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模板详细内容（03）Controller
 *
 * @author xianyue
 * @date 2021-01-13
 */
@RestController
@RequestMapping("/normal/dudao/tempItem")
public class TempItemController extends BaseController {
    @Autowired
    private IDdTempItemService ddTempItemService;

    /**
     * 查询模板详细内容（03）列表
     */
    @PreAuthorize("@ss.hasPermi('system:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(DdTempItem ddTempItem) {
        startPage();
        List<DdTempItem> list = ddTempItemService.selectDdTempItemList(ddTempItem);
        return getDataTable(list);
    }

    /**
     * 导出模板详细内容（03）列表
     */
    @PreAuthorize("@ss.hasPermi('system:item:export')")
    @Log(title = "模板详细内容（03）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdTempItem ddTempItem) {
        List<DdTempItem> list = ddTempItemService.selectDdTempItemList(ddTempItem);
        ExcelUtil<DdTempItem> util = new ExcelUtil<DdTempItem>(DdTempItem.class);
        return util.exportExcel(list, "item");
    }

    /**
     * 获取模板详细内容（03）详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(ddTempItemService.selectDdTempItemById(id));
    }

    /**
     * 新增模板详细内容（03）
     */
    @PreAuthorize("@ss.hasPermi('system:item:add')")
    @Log(title = "模板详细内容（03）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdTempItem ddTempItem) {
        return toAjax(ddTempItemService.insertDdTempItem(ddTempItem));
    }

    /**
     * 修改模板详细内容（03）
     */
    @PreAuthorize("@ss.hasPermi('system:item:edit')")
    @Log(title = "模板详细内容（03）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdTempItem ddTempItem) {
        return toAjax(ddTempItemService.updateDdTempItem(ddTempItem));
    }

    /**
     * 删除模板详细内容（03）
     */
    @PreAuthorize("@ss.hasPermi('system:item:remove')")
    @Log(title = "模板详细内容（03）", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(ddTempItemService.deleteDdTempItemByIds(ids));
    }
}
