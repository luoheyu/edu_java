package com.travel.controller.dudao;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.DdTempCate;
import com.travel.system.service.IDdTempCateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模板分类（02）Controller
 *
 * @author xianyue
 * @date 2021-01-12
 */
@RestController
@RequestMapping("/normal/tempCate")
public class TempCateController extends BaseController {
    @Autowired
    private IDdTempCateService ddTempCateService;

    /**
     * 查询模板分类（02）列表
     */
    @PreAuthorize("@ss.hasPermi('system:cate:list')")
    @GetMapping("/list")
    public TableDataInfo list(DdTempCate ddTempCate) {
        startPage();
        List<DdTempCate> list = ddTempCateService.selectDdTempCateList(ddTempCate);
        return getDataTable(list);
    }

    /**
     * 导出模板分类（02）列表
     */
    @PreAuthorize("@ss.hasPermi('system:cate:export')")
    @Log(title = "模板分类（02）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdTempCate ddTempCate) {
        List<DdTempCate> list = ddTempCateService.selectDdTempCateList(ddTempCate);
        ExcelUtil<DdTempCate> util = new ExcelUtil<DdTempCate>(DdTempCate.class);
        return util.exportExcel(list, "cate");
    }

    /**
     * 获取模板分类（02）详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:cate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(ddTempCateService.selectDdTempCateById(id));
    }

    /**
     * 新增模板分类（02）
     */
    @PreAuthorize("@ss.hasPermi('system:cate:add')")
    @Log(title = "模板分类（02）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdTempCate ddTempCate) {
        return toAjax(ddTempCateService.insertDdTempCate(ddTempCate));
    }

    /**
     * 修改模板分类（02）
     */
    @PreAuthorize("@ss.hasPermi('system:cate:edit')")
    @Log(title = "模板分类（02）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdTempCate ddTempCate) {
        return toAjax(ddTempCateService.updateDdTempCate(ddTempCate));
    }

    /**
     * 删除模板分类（02）
     */
    @PreAuthorize("@ss.hasPermi('system:cate:remove')")
    @Log(title = "模板分类（02）", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(ddTempCateService.deleteDdTempCateByIds(ids));
    }
    /**
    *根据模板id获取所有的分类
    *
    */
    @GetMapping("/getCatesByTempId/{tempId}")
    public AjaxResult getCatesByTempId(@PathVariable Integer tempId){
        List<DdTempCate> list = ddTempCateService.getCatesByTempId(tempId);
        return AjaxResult.success(list);
    }
}
