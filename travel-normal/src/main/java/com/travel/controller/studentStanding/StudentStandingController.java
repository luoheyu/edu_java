package com.travel.controller.studentStanding;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.StringUtils;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzStudentStanding;
import com.travel.system.service.IEduStudentService;
import com.travel.system.service.ITzStudentStandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 6-16周岁台账Controller
 *
 * @author xianyue
 * @date 2021-02-03
 */
@RestController
@RequestMapping("/normal/studentStanding")
public class StudentStandingController extends BaseController {
    @Autowired
    private ITzStudentStandingService tzStudentStandingService;
    @Autowired
    private IEduStudentService eduStudentService;

    /**
     * 查询6-16周岁台账列表
     */
//    @PreAuthorize("@ss.hasPermi('system:standing6:list')")
    @GetMapping("/list")
    public TableDataInfo list(TzStudentStanding tzStudentStanding) {
        startPage();
        List<TzStudentStanding> list = tzStudentStandingService.selectTzStudentStandingList(tzStudentStanding);
        return getDataTable(list);
    }

    /**
     * 导出6-16周岁台账列表
     */
    @PreAuthorize("@ss.hasPermi('system:standing6:export')")
    @Log(title = "6-16周岁台账", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TzStudentStanding tzStudentStanding) {
        List<TzStudentStanding> list = tzStudentStandingService.selectTzStudentStandingList(tzStudentStanding);
        ExcelUtil<TzStudentStanding> util = new ExcelUtil<TzStudentStanding>(TzStudentStanding.class);
        return util.exportExcel(list, "standing");
    }

    /**
     * 获取6-16周岁台账详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:standing6:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(tzStudentStandingService.selectTzStudentStandingById(id));
    }

    /**
     * 新增6-16周岁台账
     */
    @PreAuthorize("@ss.hasPermi('system:standing6:add')")
    @Log(title = "6-16周岁台账", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TzStudentStanding tzStudentStanding) {
        return toAjax(tzStudentStandingService.insertTzStudentStanding(tzStudentStanding));
    }

    /**
     * 修改6-16周岁台账
     */
    @PreAuthorize("@ss.hasPermi('system:standing6:edit')")
    @Log(title = "6-16周岁台账", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TzStudentStanding tzStudentStanding) {
        return toAjax(tzStudentStandingService.updateTzStudentStanding(tzStudentStanding));
    }

    /**
     * 删除6-16周岁台账
     */
    @PreAuthorize("@ss.hasPermi('system:standing6:remove')")
    @Log(title = "6-16周岁台账", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(tzStudentStandingService.deleteTzStudentStandingByIds(ids));
    }

    /**
     * 获取统计的年份
     */
    @GetMapping("/getTotalYear/{tName}")
    public AjaxResult getTotalYear(@PathVariable("tName") String tName) {
        List<TzStudentStanding> list = tzStudentStandingService.getTotalYear(tName);
        AjaxResult success = AjaxResult.success(list);
        return success;
    }

    /**
     * 生成6-16台账
     */
    @GetMapping("/createTotal/{tName}/{y}")
    public AjaxResult createTotal(@PathVariable("tName") String tName, @PathVariable("y") Integer y) {
        //获取当前年份
        Calendar date = Calendar.getInstance();
        Integer year = Integer.valueOf(date.get(Calendar.YEAR));
        String year6_s = String.valueOf(year - y) + "-08-31 00:00:00";
        String year16_s = String.valueOf(year - y - 1) + "-08-31 23:59:59";
        //把字符串日期转变为日期类数据
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        List<EduStudent> list = new ArrayList<>();
        AjaxResult res;
        try {
            Date year6 = format.parse(year6_s);
            Date year16 = format.parse(year16_s);
            //根据日期获取满足6-16周岁的学生
            list = eduStudentService.getStudentByDate(year6, year16);
            if (StringUtils.isEmpty(list)) {
                res = AjaxResult.error("暂无符合条件学生！");
            } else {
                tzStudentStandingService.createTotal(list,tName);
                res = AjaxResult.success();
            }
        } catch (ParseException e) {
            res = AjaxResult.error();
            e.printStackTrace();
        }
        return res;
    }
}
