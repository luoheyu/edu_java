package com.travel.controller.dateView;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.EduStudent;
import com.travel.system.domain.EduZone;
import com.travel.system.domain.TzCitytotal;
import com.travel.system.service.IEduStudentService;
import com.travel.system.service.IEduZoneService;
import com.travel.system.service.ITzCitytotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 适龄儿童统计（市）Controller
 *
 * @author xianyue
 * @date 2021-02-05
 */
@RestController
@RequestMapping("/normal/dataView")
public class DataViewController extends BaseController {
    @Autowired
    private ITzCitytotalService tzCitytotalService;
    @Autowired
    private IEduZoneService iEduZoneService;
    @Autowired
    private IEduStudentService iEduStudentService;

    /**
     * 查询适龄儿童统计（市）列表
     */
    @PreAuthorize("@ss.hasPermi('system:citytotal:list')")
    @GetMapping("/list")
    public TableDataInfo list(TzCitytotal tzCitytotal) {
        startPage();
        List<TzCitytotal> list = tzCitytotalService.selectTzCitytotalList(tzCitytotal);
        return getDataTable(list);
    }

    /**
     * 生成适龄儿童统计（市）
     */
    @GetMapping("/createCityTotal")
    public AjaxResult createCityTotal() {
        Calendar date = Calendar.getInstance();
        Integer year = Integer.valueOf(date.get(Calendar.YEAR));
        String year6_s = String.valueOf(year - 6) + "-08-31 00:00:00";
        String year16_s = String.valueOf(year - 17) + "-08-31 23:59:59";
        //把字符串日期转变为日期类数据
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        AjaxResult res;
        //获取全部的镇
        List<EduZone> zoneList = iEduZoneService.getZoneList(0);
        List<TzCitytotal> list = new ArrayList<>();
        try {
            Date year6 = format.parse(year6_s);
            Date year16 = format.parse(year16_s);
            for (EduZone v : zoneList) {
                //建档立卡户数统计
                Integer record = iEduStudentService.statisticalTotal(year6, year16, v.getId());
                //根据镇id获取学生列表
                List<EduStudent> studentList = iEduStudentService.getStudentByDateAndZone(year6, year16, v.getId());

            }
            res = AjaxResult.success();
        } catch (ParseException e) {
            res = AjaxResult.error();
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 导出适龄儿童统计（市）列表
     */
    @PreAuthorize("@ss.hasPermi('system:citytotal:export')")
    @Log(title = "适龄儿童统计（市）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TzCitytotal tzCitytotal) {
        List<TzCitytotal> list = tzCitytotalService.selectTzCitytotalList(tzCitytotal);
        ExcelUtil<TzCitytotal> util = new ExcelUtil<TzCitytotal>(TzCitytotal.class);
        return util.exportExcel(list, "citytotal");
    }

    /**
     * 获取适龄儿童统计（市）详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:citytotal:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(tzCitytotalService.selectTzCitytotalById(id));
    }

    /**
     * 新增适龄儿童统计（市）
     */
    @PreAuthorize("@ss.hasPermi('system:citytotal:add')")
    @Log(title = "适龄儿童统计（市）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TzCitytotal tzCitytotal) {
        return toAjax(tzCitytotalService.insertTzCitytotal(tzCitytotal));
    }

    /**
     * 修改适龄儿童统计（市）
     */
    @PreAuthorize("@ss.hasPermi('system:citytotal:edit')")
    @Log(title = "适龄儿童统计（市）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TzCitytotal tzCitytotal) {
        return toAjax(tzCitytotalService.updateTzCitytotal(tzCitytotal));
    }

    /**
     * 删除适龄儿童统计（市）
     */
    @PreAuthorize("@ss.hasPermi('system:citytotal:remove')")
    @Log(title = "适龄儿童统计（市）", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(tzCitytotalService.deleteTzCitytotalByIds(ids));
    }
}
