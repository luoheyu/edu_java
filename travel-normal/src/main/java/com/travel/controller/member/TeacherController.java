package com.travel.controller.member;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.EduTeacher;
import com.travel.system.service.IEduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 教师管理Controller
 *
 * @author xianyue
 * @date 2021-01-06
 */
@RestController
@RequestMapping("/normal/teacher")
public class TeacherController extends BaseController {
    @Autowired
    private IEduTeacherService eduTeacherService;

    /**
     * 查询教师管理列表
     */
    @PreAuthorize("@ss.hasPermi('normal:teacher:list')")
    @GetMapping("/list")
    public TableDataInfo list(EduTeacher eduTeacher) {
        startPage();
        List<EduTeacher> list = eduTeacherService.selectEduTeacherList(eduTeacher);
        return getDataTable(list);
    }

    /**
     * 导出教师管理列表
     */
    @PreAuthorize("@ss.hasPermi('normal:teacher:export')")
    @Log(title = "教师管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EduTeacher eduTeacher) {
        List<EduTeacher> list = eduTeacherService.selectEduTeacherList(eduTeacher);
        ExcelUtil<EduTeacher> util = new ExcelUtil<EduTeacher>(EduTeacher.class);
        return util.exportExcel(list, "teacher");
    }

    /**
     * 获取教师管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('normal:teacher:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(eduTeacherService.selectEduTeacherById(id));
    }

    /**
     * 新增教师管理
     */
    @PreAuthorize("@ss.hasPermi('normal:teacher:add')")
    @Log(title = "教师管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EduTeacher eduTeacher) {
        return toAjax(eduTeacherService.insertEduTeacher(eduTeacher));
    }

    /**
     * 修改教师管理
     */
    @PreAuthorize("@ss.hasPermi('normal:teacher:edit')")
    @Log(title = "教师管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EduTeacher eduTeacher) {
        return toAjax(eduTeacherService.updateEduTeacher(eduTeacher));
    }

    /**
     * 删除教师管理
     */
    @PreAuthorize("@ss.hasPermi('normal:teacher:remove')")
    @Log(title = "教师管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(eduTeacherService.deleteEduTeacherByIds(ids));
    }
}
