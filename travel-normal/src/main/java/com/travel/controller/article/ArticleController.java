package com.travel.controller.article;

import com.travel.common.annotation.Log;
import com.travel.common.core.controller.BaseController;
import com.travel.common.core.domain.AjaxResult;
import com.travel.common.core.page.TableDataInfo;
import com.travel.common.enums.BusinessType;
import com.travel.common.utils.poi.ExcelUtil;
import com.travel.system.domain.EduArticle;
import com.travel.system.service.IEduArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 政策文件Controller
 *
 * @author yu
 * @date 2021-01-08
 */
@RestController
@RequestMapping("/normal/article")
public class ArticleController extends BaseController {
    @Autowired
    private IEduArticleService eduArticleService;

    /**
     * 查询政策文件列表
     */
    @PreAuthorize("@ss.hasPermi('system:article:list')")
    @GetMapping("/list")
    public TableDataInfo list(EduArticle eduArticle) {
        startPage();
        List<EduArticle> list = eduArticleService.selectEduArticleList(eduArticle);
        return getDataTable(list);
    }

    /**
     * 导出政策文件列表
     */
    @PreAuthorize("@ss.hasPermi('system:article:export')")
    @Log(title = "政策文件", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EduArticle eduArticle) {
        List<EduArticle> list = eduArticleService.selectEduArticleList(eduArticle);
        ExcelUtil<EduArticle> util = new ExcelUtil<EduArticle>(EduArticle.class);
        return util.exportExcel(list, "article");
    }

    /**
     * 获取政策文件详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:article:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(eduArticleService.selectEduArticleById(id));
    }

    /**
     * 新增政策文件
     */
    @PreAuthorize("@ss.hasPermi('system:article:add')")
    @Log(title = "政策文件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EduArticle eduArticle) {
        return toAjax(eduArticleService.insertEduArticle(eduArticle));
    }

    /**
     * 修改政策文件
     */
    @PreAuthorize("@ss.hasPermi('system:article:edit')")
    @Log(title = "政策文件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EduArticle eduArticle) {
        return toAjax(eduArticleService.updateEduArticle(eduArticle));
    }

    /**
     * 删除政策文件
     */
    @PreAuthorize("@ss.hasPermi('system:article:remove')")
    @Log(title = "政策文件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(eduArticleService.deleteEduArticleByIds(ids));
    }
}
