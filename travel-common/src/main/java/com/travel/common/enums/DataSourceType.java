package com.travel.common.enums;

/**
 * 数据源
 * 
 * @author xianyue
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
