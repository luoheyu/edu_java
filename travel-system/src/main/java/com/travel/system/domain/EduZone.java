package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.annotation.Excels;
import com.travel.common.core.domain.TreeEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 村镇管理对象 edu_zone
 *
 * @author yu
 * @date 2020-12-23
 */
public class EduZone extends TreeEntity {

    private static final long serialVersionUID = 1L;


    /**
     * $column.columnComment
     */
    private Integer id;


    /**
     * 父id
     */
    @Excel(name = "父id")
    private Integer fid;


    /**
     * 村镇名称
     */
    @Excel(name = "村镇名称")
    private String name;


    /**
     * 所属初中
     */
    @Excel(name = "所属初中")
    private Integer middleSchoolId;


    //    所属初中对象
    @Excels({
            @Excel(name = "所属中学", targetAttr = "schoolName", type = Excel.Type.EXPORT)
    })
    private EduSchool middle;

    //    所属初中对象
    @Excels({
            @Excel(name = "所属小学", targetAttr = "schoolName", type = Excel.Type.EXPORT)
    })
    private EduSchool young;

    /**
     * 所属小学
     */
    @Excel(name = "所属小学")
    private Integer youngSchoolId;


    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getId() {
        return id;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }


    public Integer getFid() {
        return fid;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setMiddleSchoolId(Integer middleSchoolId) {
        this.middleSchoolId = middleSchoolId;
    }


    public Integer getMiddleSchoolId() {
        return middleSchoolId;
    }

    public void setYoungSchoolId(Integer youngSchoolId) {
        this.youngSchoolId = youngSchoolId;
    }


    public Integer getYoungSchoolId() {
        return youngSchoolId;
    }

    public EduSchool getMiddle() {
        return middle;
    }

    public void setMiddle(EduSchool middle) {
        this.middle = middle;
    }

    public EduSchool getYoung() {
        return young;
    }

    public void setYoung(EduSchool young) {
        this.young = young;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("fid", getFid())
                .append("name", getName())
                .append("middleSchoolId", getMiddleSchoolId())
                .append("youngSchoolId", getYoungSchoolId())
                .append("middle", getMiddle())
                .append("young", getYoung())
                .toString();
    }
}

