package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class EduArticle extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 标题
     */
    @Excel(name = "标题")
    private String title;
    /**
     * 副标题
     */
    @Excel(name = "副标题")
    private String titleSecond;
    /**
     * 简单描述
     */
    @Excel(name = "简单描述")
    private String intro;
    /**
     * 内容
     */
    @Excel(name = "内容")
    private String content;
    /**
     * 附件
     */
    @Excel(name = "附件")
    private String attachFile;
    /**
     * 附件小图
     */
    @Excel(name = "附件小图")
    private String attachThumb;
    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sortOrder;
    /**
     * 查看次数
     */
    @Excel(name = "查看次数")
    private Integer viewCount;
    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer statusIs;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitleSecond(String titleSecond) {
        this.titleSecond = titleSecond;
    }

    public String getTitleSecond() {
        return titleSecond;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getIntro() {
        return intro;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setAttachFile(String attachFile) {
        this.attachFile = attachFile;
    }

    public String getAttachFile() {
        return attachFile;
    }

    public void setAttachThumb(String attachThumb) {
        this.attachThumb = attachThumb;
    }

    public String getAttachThumb() {
        return attachThumb;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setStatusIs(Integer statusIs) {
        this.statusIs = statusIs;
    }

    public Integer getStatusIs() {
        return statusIs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("title", getTitle())
                .append("titleSecond", getTitleSecond())
                .append("intro", getIntro())
                .append("content", getContent())
                .append("attachFile", getAttachFile())
                .append("attachThumb", getAttachThumb())
                .append("sortOrder", getSortOrder())
                .append("viewCount", getViewCount())
                .append("statusIs", getStatusIs())
                .append("createTime", getCreateTime())
                .toString();
    }
}
