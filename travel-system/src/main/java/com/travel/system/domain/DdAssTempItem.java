package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 模板考核，各个学校考核记录之单个条目记录对象 dd_ass_temp_item
 *
 * @author xianyue
 * @date 2021-01-14
 */
public class DdAssTempItem extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 模板考核结果记录表id
     */
    @Excel(name = "模板考核结果记录表id")
    private Integer assTempId;
    /**
     * 所属模板id
     */
    @Excel(name = "所属模板id")
    private Integer tempId;
    /**
     * 所属模板分类id
     */
    @Excel(name = "所属模板分类id")
    private Integer tempCateId;
    /**
     * 所属模板细想id
     */
    @Excel(name = "所属模板细想id")
    private Integer tempItemId;
    /**
     * 考核问题描述
     */
    @Excel(name = "考核问题描述")
    private String question;
    /**
     * 问题图片
     */
    @Excel(name = "问题图片")
    private String questionImgs;
    /**
     * 问题反馈
     */
    @Excel(name = "问题反馈")
    private String feedback;
    /**
     * 反馈图片
     */
    @Excel(name = "反馈图片")
    private String feedbackImgs;
    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer status;
    /**
     * 得分
     */
    @Excel(name = "得分")
    private Integer getScore;

    //所属item细则
    private DdTempItem item;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setAssTempId(Integer assTempId) {
        this.assTempId = assTempId;
    }

    public Integer getAssTempId() {
        return assTempId;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    public Integer getTempId() {
        return tempId;
    }

    public void setTempCateId(Integer tempCateId) {
        this.tempCateId = tempCateId;
    }

    public Integer getTempCateId() {
        return tempCateId;
    }

    public void setTempItemId(Integer tempItemId) {
        this.tempItemId = tempItemId;
    }

    public Integer getTempItemId() {
        return tempItemId;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestionImgs(String questionImgs) {
        this.questionImgs = questionImgs;
    }

    public String getQuestionImgs() {
        return questionImgs;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedbackImgs(String feedbackImgs) {
        this.feedbackImgs = feedbackImgs;
    }

    public String getFeedbackImgs() {
        return feedbackImgs;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setGetScore(Integer getScore) {
        this.getScore = getScore;
    }

    public Integer getGetScore() {
        return getScore;
    }

    public DdTempItem getItem() {
        return item;
    }

    public void setItem(DdTempItem item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("assTempId", getAssTempId())
                .append("tempId", getTempId())
                .append("tempCateId", getTempCateId())
                .append("tempItemId", getTempItemId())
                .append("question", getQuestion())
                .append("questionImgs", getQuestionImgs())
                .append("feedback", getFeedback())
                .append("feedbackImgs", getFeedbackImgs())
                .append("status", getStatus())
                .append("remark", getRemark())
                .append("createTime", getCreateTime())
                .append("getScore", getGetScore())
                .toString();
    }
}
