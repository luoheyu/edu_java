package com.travel.system.domain.DataModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.travel.system.domain.EduSchool;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * schooltreeselect树结构实体类
 */
public class SchoolTreeSelect implements Serializable {

    /**
     * 节点ID
     */
    private Integer id;

    /**
     * 节点名称
     */
    private String label;

    /**
     * 子节点
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<SchoolTreeSelect> children;

    public SchoolTreeSelect() {

    }

    public SchoolTreeSelect(EduSchool school) {
        this.id = school.getId();
        this.label = school.getSchoolName();
        this.children = school.getChildren().stream().map(SchoolTreeSelect::new).collect(Collectors.toList());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<SchoolTreeSelect> getChildren() {
        return children;
    }

    public void setChildren(List<SchoolTreeSelect> children) {
        this.children = children;
    }
}
