package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 模板名（01）对象 dd_temp_name
 *
 * @author xianyue
 * @date 2021-01-11
 */
public class DdTempName extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 模板名称
     */
    @Excel(name = "模板名称")
    private String tempName;
    /**
     * 是否启用
     */
    @Excel(name = "是否启用")
    private Integer isUse;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setTempName(String tempName) {
        this.tempName = tempName;
    }

    public String getTempName() {
        return tempName;
    }

    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    public Integer getIsUse() {
        return isUse;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("tempName", getTempName())
                .append("isUse", getIsUse())
                .append("createTime", getCreateTime())
                .toString();
    }
}
