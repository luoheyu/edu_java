package com.travel.system.domain;
import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 教师管理对象 edu_teacher
 *
 * @author xianyue
 * @date 2021-01-06
 */
public class EduTeacher extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Long id;
    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;
    /**
     * 性别
     */
    @Excel(name = "性别")
    private Integer sex;
    /**
     * 年龄
     */
    @Excel(name = "年龄")
    private Integer age;
    /**
     * kid（绑定捷成）
     */
    @Excel(name = "kid", readConverterExp = "绑=定捷成")
    private String kid;
    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer status;
    /**
     * 所属学校
     */
    @Excel(name = "所属学校")
    private Long schoolId;
    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setSex(Integer sex) {
        this.sex = sex;
    }
    public Integer getSex() {
        return sex;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public Integer getAge() {
        return age;
    }
    public void setKid(String kid) {
        this.kid = kid;
    }
    public String getKid() {
        return kid;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getStatus() {
        return status;
    }
    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }
    public Long getSchoolId() {
        return schoolId;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("sex", getSex())
                .append("age", getAge())
                .append("kid", getKid())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("schoolId", getSchoolId())
                .toString();
    }
}
