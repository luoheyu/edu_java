package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 模板考核，各个学校考核记录对象 dd_ass_temp
 *
 * @author xianyue
 * @date 2021-01-14
 */
public class DdAssTemp extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 学校id
     */
    @Excel(name = "学校id")
    private Integer schoolId;
    /**
     * 学校名
     */
    @Excel(name = "学校名")
    private String schoolName;
    /**
     * 模板id
     */
    @Excel(name = "模板id")
    private Integer tempId;
    /**
     * 模板名
     */
    @Excel(name = "模板名")
    private String tempName;
    /**
     * 督导id
     */
    @Excel(name = "督导id")
    private Integer dudaoId;
    /**
     * 督导名称
     */
    @Excel(name = "督导名称")
    private String dudaoName;

    private Date createTime;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    public Integer getTempId() {
        return tempId;
    }

    public void setTempName(String tempName) {
        this.tempName = tempName;
    }

    public String getTempName() {
        return tempName;
    }

    public void setDudaoId(Integer dudaoId) {
        this.dudaoId = dudaoId;
    }

    public Integer getDudaoId() {
        return dudaoId;
    }

    public void setDudaoName(String dudaoName) {
        this.dudaoName = dudaoName;
    }

    public String getDudaoName() {
        return dudaoName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("schoolId", getSchoolId())
                .append("schoolName", getSchoolName())
                .append("tempId", getTempId())
                .append("tempName", getTempName())
                .append("dudaoId", getDudaoId())
                .append("dudaoName", getDudaoName())
                .append("createTime", getCreateTime())
                .toString();
    }
}
