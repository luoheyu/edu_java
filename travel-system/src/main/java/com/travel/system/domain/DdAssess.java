package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 补充内容表对象 dd_assess
 *
 * @author xianyue
 * @date 2021-01-18
 */
public class DdAssess extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 学校id
     */
    @Excel(name = "学校id")
    private Integer schoolId;
    /**
     * 学校名称
     */
    @Excel(name = "学校名称")
    private String schoolName;
    /**
     * 督导员id
     */
    @Excel(name = "督导员id")
    private Integer dudaoId;
    /**
     * 督导员名字
     */
    @Excel(name = "督导员名字")
    private String dudaoName;
    /**
     * 考核模板id
     */
    @Excel(name = "考核模板id")
    private Integer tempId;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setDudaoId(Integer dudaoId) {
        this.dudaoId = dudaoId;
    }

    public Integer getDudaoId() {
        return dudaoId;
    }

    public void setDudaoName(String dudaoName) {
        this.dudaoName = dudaoName;
    }

    public String getDudaoName() {
        return dudaoName;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    public Integer getTempId() {
        return tempId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("schoolId", getSchoolId())
                .append("schoolName", getSchoolName())
                .append("dudaoId", getDudaoId())
                .append("dudaoName", getDudaoName())
                .append("createTime", getCreateTime())
                .append("tempId", getTempId())
                .toString();
    }
}
