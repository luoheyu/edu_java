package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 适龄儿童统计（市）对象 tz_citytotal
 *
 * @author xianyue
 * @date 2021-02-05
 */
public class TzCitytotal extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 街道名称
     */
    @Excel(name = "街道名称")
    private String street;
    /**
     * 街道id
     */
    @Excel(name = "街道id")
    private Integer streetId;
    /**
     * 街道适龄儿童统计
     */
    @Excel(name = "街道适龄儿童统计")
    private Integer total;
    /**
     * 建档立卡户数统计
     */
    @Excel(name = "建档立卡户数统计")
    private Integer recordHouseholdTotal;
    /**
     * 建档立卡人数统计
     */
    @Excel(name = "建档立卡人数统计")
    private Integer recordPeopleTotal;
    /**
     * 非建档立卡农户户数
     */
    @Excel(name = "非建档立卡农户户数")
    private Integer notRecordHouseholdTotal;
    /**
     * 非建档立卡农户学生数
     */
    @Excel(name = "非建档立卡农户学生数")
    private Integer notRecordPeopleTotal;
    /**
     * 6周儿童统计
     */
    @Excel(name = "6周儿童统计")
    private Integer year6;
    /**
     * 7周儿童统计
     */
    @Excel(name = "7周儿童统计")
    private Integer year7;
    /**
     * 8周儿童统计
     */
    @Excel(name = "8周儿童统计")
    private Integer year8;
    /**
     * 9周儿童统计
     */
    @Excel(name = "9周儿童统计")
    private Integer year9;
    /**
     * 10周儿童统计
     */
    @Excel(name = "10周儿童统计")
    private Integer year10;
    /**
     * 11周儿童统计
     */
    @Excel(name = "11周儿童统计")
    private Integer year11;
    /**
     * 12周儿童统计
     */
    @Excel(name = "12周儿童统计")
    private Integer year12;
    /**
     * 13周儿童统计
     */
    @Excel(name = "13周儿童统计")
    private Integer year13;
    /**
     * 14周儿童统计
     */
    @Excel(name = "14周儿童统计")
    private Integer year14;
    /**
     * 15周儿童统计
     */
    @Excel(name = "15周儿童统计")
    private Integer year15;
    /**
     * 16周儿童统计
     */
    @Excel(name = "16周儿童统计")
    private Integer year16;
    /**
     * 统计年份
     */
    @Excel(name = "统计年份")
    private Integer totalYear;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setRecordHouseholdTotal(Integer recordHouseholdTotal) {
        this.recordHouseholdTotal = recordHouseholdTotal;
    }

    public Integer getRecordHouseholdTotal() {
        return recordHouseholdTotal;
    }

    public void setRecordPeopleTotal(Integer recordPeopleTotal) {
        this.recordPeopleTotal = recordPeopleTotal;
    }

    public Integer getRecordPeopleTotal() {
        return recordPeopleTotal;
    }

    public void setNotRecordHouseholdTotal(Integer notRecordHouseholdTotal) {
        this.notRecordHouseholdTotal = notRecordHouseholdTotal;
    }

    public Integer getNotRecordHouseholdTotal() {
        return notRecordHouseholdTotal;
    }

    public void setNotRecordPeopleTotal(Integer notRecordPeopleTotal) {
        this.notRecordPeopleTotal = notRecordPeopleTotal;
    }

    public Integer getNotRecordPeopleTotal() {
        return notRecordPeopleTotal;
    }

    public void setYear6(Integer year6) {
        this.year6 = year6;
    }

    public Integer getYear6() {
        return year6;
    }

    public void setYear7(Integer year7) {
        this.year7 = year7;
    }

    public Integer getYear7() {
        return year7;
    }

    public void setYear8(Integer year8) {
        this.year8 = year8;
    }

    public Integer getYear8() {
        return year8;
    }

    public void setYear9(Integer year9) {
        this.year9 = year9;
    }

    public Integer getYear9() {
        return year9;
    }

    public void setYear10(Integer year10) {
        this.year10 = year10;
    }

    public Integer getYear10() {
        return year10;
    }

    public void setYear11(Integer year11) {
        this.year11 = year11;
    }

    public Integer getYear11() {
        return year11;
    }

    public void setYear12(Integer year12) {
        this.year12 = year12;
    }

    public Integer getYear12() {
        return year12;
    }

    public void setYear13(Integer year13) {
        this.year13 = year13;
    }

    public Integer getYear13() {
        return year13;
    }

    public void setYear14(Integer year14) {
        this.year14 = year14;
    }

    public Integer getYear14() {
        return year14;
    }

    public void setYear15(Integer year15) {
        this.year15 = year15;
    }

    public Integer getYear15() {
        return year15;
    }

    public void setYear16(Integer year16) {
        this.year16 = year16;
    }

    public Integer getYear16() {
        return year16;
    }

    public void setTotalYear(Integer totalYear) {
        this.totalYear = totalYear;
    }

    public Integer getTotalYear() {
        return totalYear;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("street", getStreet())
                .append("streetId", getStreetId())
                .append("total", getTotal())
                .append("recordHouseholdTotal", getRecordHouseholdTotal())
                .append("recordPeopleTotal", getRecordPeopleTotal())
                .append("notRecordHouseholdTotal", getNotRecordHouseholdTotal())
                .append("notRecordPeopleTotal", getNotRecordPeopleTotal())
                .append("year6", getYear6())
                .append("year7", getYear7())
                .append("year8", getYear8())
                .append("year9", getYear9())
                .append("year10", getYear10())
                .append("year11", getYear11())
                .append("year12", getYear12())
                .append("year13", getYear13())
                .append("year14", getYear14())
                .append("year15", getYear15())
                .append("year16", getYear16())
                .append("totalYear", getTotalYear())
                .toString();
    }
}
