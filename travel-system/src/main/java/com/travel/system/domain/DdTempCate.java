package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 模板分类（02）对象 dd_temp_cate
 *
 * @author xianyue
 * @date 2021-01-12
 */
public class DdTempCate extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 所属模板
     */
    @Excel(name = "所属模板")
    private Integer tempId;
    /**
     * 分类名称
     */
    @Excel(name = "分类名称")
    private String name;
    /**
     * 分数
     */
    @Excel(name = "分数")
    private Integer score;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    public Integer getTempId() {
        return tempId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getScore() {
        return score;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("tempId", getTempId())
                .append("name", getName())
                .append("score", getScore())
                .toString();
    }
}
