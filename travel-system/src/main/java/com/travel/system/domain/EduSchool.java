package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * 【请填写功能名称】对象 edu_school
 *
 * @author xianyue
 * @date 2020-12-21
 */
public class EduSchool extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 父id
     */
    @Excel(name = "父id")
    private Integer fid;
    /**
     * 镇id
     */
    @Excel(name = "镇id")
    private Integer townId;
    /**
     * 村id
     */
    @Excel(name = "村id")
    private Integer villageId;
    /**
     * 学校类型
     */
    @Excel(name = "学校类型")
    private Integer schoolType;
    /**
     * 学校名称
     */
    @Excel(name = "学校名称")
    private String schoolName;
    /**
     * 子学校
     */
    private List<EduSchool> children = new ArrayList<EduSchool>();

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public Integer getFid() {
        return fid;
    }

    public void setTownId(Integer townId) {
        this.townId = townId;
    }

    public Integer getTownId() {
        return townId;
    }

    public void setVillageId(Integer villageId) {
        this.villageId = villageId;
    }

    public Integer getVillageId() {
        return villageId;
    }

    public void setSchoolType(Integer schoolType) {
        this.schoolType = schoolType;
    }

    public Integer getSchoolType() {
        return schoolType;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public List<EduSchool> getChildren() {
        return children;
    }

    public void setChildren(List<EduSchool> childen) {
        this.children = childen;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("fid", getFid())
                .append("townId", getTownId())
                .append("villageId", getVillageId())
                .append("schoolType", getSchoolType())
                .append("schoolName", getSchoolName())
                .append("children", getChildren())
                .toString();
    }
}
