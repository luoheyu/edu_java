package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 6-16周岁台账对象 tz_student_standing
 *
 * @author xianyue
 * @date 2021-02-03
 */
public class TzStudentStanding extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;
    /**
     * 生日
     */
    @Excel(name = "生日")
    private Integer birth;
    /**
     * 身份证号
     */
    @Excel(name = "身份证号")
    private String idcard;
    /**
     * 性别 1男 2女
     */
    @Excel(name = "性别 1男 2女")
    private Integer sex;
    /**
     * 是否农户
     */
    @Excel(name = "是否农户")
    private Integer farmers;
    /**
     * 镇id
     */
    @Excel(name = "镇id")
    private Integer townId;
    /**
     * 村id
     */
    @Excel(name = "村id")
    private Integer villageId;
    /**
     * 详细地址
     */
    @Excel(name = "详细地址")
    private String address;
    /**
     * 所属中学
     */
    @Excel(name = "所属中学")
    private Integer middleSchool;
    /**
     * 所属小学
     */
    @Excel(name = "所属小学")
    private Integer youngSchool;
    /**
     * 监护人
     */
    @Excel(name = "监护人")
    private String guardian;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String phone;
    /**
     * 是否建档
     */
    @Excel(name = "是否建档")
    private Integer record;
    /**
     * 是否边缘户
     */
    @Excel(name = "是否边缘户")
    private Integer edgePeople;
    /**
     * 是否残疾
     */
    @Excel(name = "是否残疾")
    private Integer disability;
    /**
     * 是否留守儿童
     */
    @Excel(name = "是否留守儿童")
    private Integer behindChildren;
    /**
     * 是否随迁子女
     */
    @Excel(name = "是否随迁子女")
    private Integer accompanyingChildren;
    /**
     * 是否延缓入学
     */
    @Excel(name = "是否延缓入学")
    private Integer isLate;
    /**
     * 是否在校
     */
    @Excel(name = "是否在校")
    private Integer inSchool;
    /**
     * 是否失学辍学
     */
    @Excel(name = "是否失学辍学")
    private Integer dropSchool;
    /**
     * 是否初中毕业
     */
    @Excel(name = "是否初中毕业")
    private Integer graduation;
    /**
     * 就读小学
     */
    @Excel(name = "就读小学")
    private String studyYoung;
    /**
     * 就读初中
     */
    @Excel(name = "就读初中")
    private String studyMiddle;
    /**
     * 是否市外省内就读
     */
    @Excel(name = "是否市外省内就读")
    private Integer outCity;
    /**
     * 是否外省就读
     */
    @Excel(name = "是否外省就读")
    private Integer outProvince;

    private String remark;

    /**
     * 统计年份
     */
    @Excel(name = "统计年份")
    private Integer totalYear;

    /**
     * 表名
     */
    private String tableName;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBirth(Integer birth) {
        this.birth = birth;
    }

    public Integer getBirth() {
        return birth;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getSex() {
        return sex;
    }

    public void setFarmers(Integer farmers) {
        this.farmers = farmers;
    }

    public Integer getFarmers() {
        return farmers;
    }

    public void setTownId(Integer townId) {
        this.townId = townId;
    }

    public Integer getTownId() {
        return townId;
    }

    public void setVillageId(Integer villageId) {
        this.villageId = villageId;
    }

    public Integer getVillageId() {
        return villageId;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setMiddleSchool(Integer middleSchool) {
        this.middleSchool = middleSchool;
    }

    public Integer getMiddleSchool() {
        return middleSchool;
    }

    public void setYoungSchool(Integer youngSchool) {
        this.youngSchool = youngSchool;
    }

    public Integer getYoungSchool() {
        return youngSchool;
    }

    public void setGuardian(String guardian) {
        this.guardian = guardian;
    }

    public String getGuardian() {
        return guardian;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setRecord(Integer record) {
        this.record = record;
    }

    public Integer getRecord() {
        return record;
    }

    public void setEdgePeople(Integer edgePeople) {
        this.edgePeople = edgePeople;
    }

    public Integer getEdgePeople() {
        return edgePeople;
    }

    public void setDisability(Integer disability) {
        this.disability = disability;
    }

    public Integer getDisability() {
        return disability;
    }

    public void setBehindChildren(Integer behindChildren) {
        this.behindChildren = behindChildren;
    }

    public Integer getBehindChildren() {
        return behindChildren;
    }

    public void setAccompanyingChildren(Integer accompanyingChildren) {
        this.accompanyingChildren = accompanyingChildren;
    }

    public Integer getAccompanyingChildren() {
        return accompanyingChildren;
    }

    public void setIsLate(Integer isLate) {
        this.isLate = isLate;
    }

    public Integer getIsLate() {
        return isLate;
    }

    public void setInSchool(Integer inSchool) {
        this.inSchool = inSchool;
    }

    public Integer getInSchool() {
        return inSchool;
    }

    public void setDropSchool(Integer dropSchool) {
        this.dropSchool = dropSchool;
    }

    public Integer getDropSchool() {
        return dropSchool;
    }

    public void setGraduation(Integer graduation) {
        this.graduation = graduation;
    }

    public Integer getGraduation() {
        return graduation;
    }

    public void setStudyYoung(String studyYoung) {
        this.studyYoung = studyYoung;
    }

    public String getStudyYoung() {
        return studyYoung;
    }

    public void setStudyMiddle(String studyMiddle) {
        this.studyMiddle = studyMiddle;
    }

    public String getStudyMiddle() {
        return studyMiddle;
    }

    public void setOutCity(Integer outCity) {
        this.outCity = outCity;
    }

    public Integer getOutCity() {
        return outCity;
    }

    public void setOutProvince(Integer outProvince) {
        this.outProvince = outProvince;
    }

    public Integer getOutProvince() {
        return outProvince;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setTotalYear(Integer totalYear) {
        this.totalYear = totalYear;
    }

    public Integer getTotalYear() {
        return totalYear;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("birth", getBirth())
                .append("idcard", getIdcard())
                .append("sex", getSex())
                .append("farmers", getFarmers())
                .append("townId", getTownId())
                .append("villageId", getVillageId())
                .append("address", getAddress())
                .append("middleSchool", getMiddleSchool())
                .append("youngSchool", getYoungSchool())
                .append("guardian", getGuardian())
                .append("phone", getPhone())
                .append("record", getRecord())
                .append("edgePeople", getEdgePeople())
                .append("disability", getDisability())
                .append("behindChildren", getBehindChildren())
                .append("accompanyingChildren", getAccompanyingChildren())
                .append("isLate", getIsLate())
                .append("inSchool", getInSchool())
                .append("dropSchool", getDropSchool())
                .append("graduation", getGraduation())
                .append("studyYoung", getStudyYoung())
                .append("studyMiddle", getStudyMiddle())
                .append("outCity", getOutCity())
                .append("outProvince", getOutProvince())
                .append("remark", getRemark())
                .append("totalYear", getTotalYear())
                .toString();
    }
}
