package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 模板详细内容（03）对象 dd_temp_item
 *
 * @author xianyue
 * @date 2021-01-13
 */
public class DdTempItem extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 模板id
     */
    @Excel(name = "模板id")
    private Integer tempId;
    /**
     * 分类id
     */
    @Excel(name = "分类id")
    private Integer tempCateId;
    /**
     * 详细条目
     */
    @Excel(name = "详细条目")
    private String item;
    /**
     * 分数
     */
    @Excel(name = "分数")
    private Integer score;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    public Integer getTempId() {
        return tempId;
    }

    public void setTempCateId(Integer tempCateId) {
        this.tempCateId = tempCateId;
    }

    public Integer getTempCateId() {
        return tempCateId;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getScore() {
        return score;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("tempId", getTempId())
                .append("tempCateId", getTempCateId())
                .append("item", getItem())
                .append("score", getScore())
                .toString();
    }
}
