package com.travel.system.domain;
import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 特殊儿童台账对象 tz_special_student_standing
 *
 * @author xianyue
 * @date 2021-01-28
 */
public class TzSpecialStudentStanding extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer id;
    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;
    /**
     * 生日
     */
    @Excel(name = "生日")
    private Integer birth;
    /**
     * 身份证号
     */
    @Excel(name = "身份证号")
    private String idcard;
    /**
     * 性别 1男 2女
     */
    @Excel(name = "性别 1男 2女")
    private Integer sex;
    /**
     * 是否农户
     */
    @Excel(name = "是否农户")
    private Integer farmers;
    /**
     * 镇id
     */
    @Excel(name = "镇id")
    private Integer townId;
    /**
     * 村id
     */
    @Excel(name = "村id")
    private Integer villageId;
    /**
     * 详细地址
     */
    @Excel(name = "详细地址")
    private String address;
    /**
     * 所属中学
     */
    @Excel(name = "所属中学")
    private Integer middleSchool;
    /**
     * 所属小学
     */
    @Excel(name = "所属小学")
    private Integer youngSchool;
    /**
     * 监护人
     */
    @Excel(name = "监护人")
    private String guardian;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String phone;
    /**
     * 是否建档
     */
    @Excel(name = "是否建档")
    private Integer record;
    /**
     * 是否边缘户
     */
    @Excel(name = "是否边缘户")
    private Integer edgePeople;
    /**
     * 是否残疾
     */
    @Excel(name = "是否残疾")
    private Integer disability;
    /**
     * 残疾类型
     */
    @Excel(name = "残疾类型")
    private String disabilityType;
    /**
     * 残疾等级
     */
    @Excel(name = "残疾等级")
    private String disabilityLevel;
    /**
     * 无接受教育能力
     */
    @Excel(name = "无接受教育能力")
    private Integer canNotStudy;
    /**
     * 学校送教
     */
    @Excel(name = "学校送教")
    private Integer schoolTeacher;
    /**
     * 特教送教
     */
    @Excel(name = "特教送教")
    private Integer speciaTeacher;
    /**
     * 特教就读
     */
    @Excel(name = "特教就读")
    private Integer specialStudy;
    /**
     * 随班就读
     */
    @Excel(name = "随班就读")
    private Integer classStudy;
    /**
     * 是否延缓入学
     */
    @Excel(name = "是否延缓入学")
    private Integer isLate;
    /**
     * 是否在校
     */
    @Excel(name = "是否在校")
    private Integer inSchool;
    /**
     * 是否失学辍学
     */
    @Excel(name = "是否失学辍学")
    private Integer dropSchool;
    /**
     * 是否初中毕业
     */
    @Excel(name = "是否初中毕业")
    private Integer graduation;
    /**
     * 就读小学
     */
    @Excel(name = "就读小学")
    private String studyYoung;
    /**
     * 就读初中
     */
    @Excel(name = "就读初中")
    private String studyMiddle;
    /**
     * 统计年份
     */
    @Excel(name = "统计年份")
    private Integer totalYear;
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setBirth(Integer birth) {
        this.birth = birth;
    }
    public Integer getBirth() {
        return birth;
    }
    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
    public String getIdcard() {
        return idcard;
    }
    public void setSex(Integer sex) {
        this.sex = sex;
    }
    public Integer getSex() {
        return sex;
    }
    public void setFarmers(Integer farmers) {
        this.farmers = farmers;
    }
    public Integer getFarmers() {
        return farmers;
    }
    public void setTownId(Integer townId) {
        this.townId = townId;
    }
    public Integer getTownId() {
        return townId;
    }
    public void setVillageId(Integer villageId) {
        this.villageId = villageId;
    }
    public Integer getVillageId() {
        return villageId;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getAddress() {
        return address;
    }
    public void setMiddleSchool(Integer middleSchool) {
        this.middleSchool = middleSchool;
    }
    public Integer getMiddleSchool() {
        return middleSchool;
    }
    public void setYoungSchool(Integer youngSchool) {
        this.youngSchool = youngSchool;
    }
    public Integer getYoungSchool() {
        return youngSchool;
    }
    public void setGuardian(String guardian) {
        this.guardian = guardian;
    }
    public String getGuardian() {
        return guardian;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPhone() {
        return phone;
    }
    public void setRecord(Integer record) {
        this.record = record;
    }
    public Integer getRecord() {
        return record;
    }
    public void setEdgePeople(Integer edgePeople) {
        this.edgePeople = edgePeople;
    }
    public Integer getEdgePeople() {
        return edgePeople;
    }
    public void setDisability(Integer disability) {
        this.disability = disability;
    }
    public Integer getDisability() {
        return disability;
    }
    public void setDisabilityType(String disabilityType) {
        this.disabilityType = disabilityType;
    }
    public String getDisabilityType() {
        return disabilityType;
    }
    public void setDisabilityLevel(String disabilityLevel) {
        this.disabilityLevel = disabilityLevel;
    }
    public String getDisabilityLevel() {
        return disabilityLevel;
    }
    public void setCanNotStudy(Integer canNotStudy) {
        this.canNotStudy = canNotStudy;
    }
    public Integer getCanNotStudy() {
        return canNotStudy;
    }
    public void setSchoolTeacher(Integer schoolTeacher) {
        this.schoolTeacher = schoolTeacher;
    }
    public Integer getSchoolTeacher() {
        return schoolTeacher;
    }
    public void setSpeciaTeacher(Integer speciaTeacher) {
        this.speciaTeacher = speciaTeacher;
    }
    public Integer getSpeciaTeacher() {
        return speciaTeacher;
    }
    public void setSpecialStudy(Integer specialStudy) {
        this.specialStudy = specialStudy;
    }
    public Integer getSpecialStudy() {
        return specialStudy;
    }
    public void setClassStudy(Integer classStudy) {
        this.classStudy = classStudy;
    }
    public Integer getClassStudy() {
        return classStudy;
    }
    public void setIsLate(Integer isLate) {
        this.isLate = isLate;
    }
    public Integer getIsLate() {
        return isLate;
    }
    public void setInSchool(Integer inSchool) {
        this.inSchool = inSchool;
    }
    public Integer getInSchool() {
        return inSchool;
    }
    public void setDropSchool(Integer dropSchool) {
        this.dropSchool = dropSchool;
    }
    public Integer getDropSchool() {
        return dropSchool;
    }
    public void setGraduation(Integer graduation) {
        this.graduation = graduation;
    }
    public Integer getGraduation() {
        return graduation;
    }
    public void setStudyYoung(String studyYoung) {
        this.studyYoung = studyYoung;
    }
    public String getStudyYoung() {
        return studyYoung;
    }
    public void setStudyMiddle(String studyMiddle) {
        this.studyMiddle = studyMiddle;
    }
    public String getStudyMiddle() {
        return studyMiddle;
    }
    public void setTotalYear(Integer totalYear) {
        this.totalYear = totalYear;
    }
    public Integer getTotalYear() {
        return totalYear;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("birth", getBirth())
                .append("idcard", getIdcard())
                .append("sex", getSex())
                .append("farmers", getFarmers())
                .append("townId", getTownId())
                .append("villageId", getVillageId())
                .append("address", getAddress())
                .append("middleSchool", getMiddleSchool())
                .append("youngSchool", getYoungSchool())
                .append("guardian", getGuardian())
                .append("phone", getPhone())
                .append("record", getRecord())
                .append("edgePeople", getEdgePeople())
                .append("disability", getDisability())
                .append("disabilityType", getDisabilityType())
                .append("disabilityLevel", getDisabilityLevel())
                .append("canNotStudy", getCanNotStudy())
                .append("schoolTeacher", getSchoolTeacher())
                .append("speciaTeacher", getSpeciaTeacher())
                .append("specialStudy", getSpecialStudy())
                .append("classStudy", getClassStudy())
                .append("isLate", getIsLate())
                .append("inSchool", getInSchool())
                .append("dropSchool", getDropSchool())
                .append("graduation", getGraduation())
                .append("studyYoung", getStudyYoung())
                .append("studyMiddle", getStudyMiddle())
                .append("remark", getRemark())
                .append("totalYear", getTotalYear())
                .toString();
    }
}
