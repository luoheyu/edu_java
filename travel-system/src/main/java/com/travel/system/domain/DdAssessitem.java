package com.travel.system.domain;

import com.travel.common.annotation.Excel;
import com.travel.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 反馈内容细项对象 dd_assessitem
 *
 * @author xianyue
 * @date 2021-01-18
 */
public class DdAssessitem extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * $column.columnComment
     */
    private Integer id;
    /**
     * 考察表id
     */
    @Excel(name = "考察表id")
    private Integer assId;
    /**
     * 问题
     */
    @Excel(name = "问题")
    private String question;
    /**
     * 问题图片
     */
    @Excel(name = "问题图片")
    private String questionImgs;
    /**
     * 问题反馈
     */
    @Excel(name = "问题反馈")
    private String feedback;
    /**
     * 反馈图片
     */
    @Excel(name = "反馈图片")
    private String feedbackImgs;
    /**
     * 状态 1待处理 2已提交 3已驳回 4已处理
     */
    @Excel(name = "状态 1待处理 2已提交 3已驳回 4已处理")
    private Integer status;
    /**
     * 所属模板id
     */
    @Excel(name = "所属模板id")
    private Integer tempId;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setAssId(Integer assId) {
        this.assId = assId;
    }

    public Integer getAssId() {
        return assId;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestionImgs(String questionImgs) {
        this.questionImgs = questionImgs;
    }

    public String getQuestionImgs() {
        return questionImgs;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedbackImgs(String feedbackImgs) {
        this.feedbackImgs = feedbackImgs;
    }

    public String getFeedbackImgs() {
        return feedbackImgs;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    public Integer getTempId() {
        return tempId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("assId", getAssId())
                .append("question", getQuestion())
                .append("questionImgs", getQuestionImgs())
                .append("feedback", getFeedback())
                .append("feedbackImgs", getFeedbackImgs())
                .append("status", getStatus())
                .append("remark", getRemark())
                .append("tempId", getTempId())
                .toString();
    }
}
