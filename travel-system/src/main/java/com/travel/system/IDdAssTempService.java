package com.travel.system;

import com.travel.system.domain.DdAssTemp;

import java.util.List;

/**
 * 模板考核，各个学校考核记录Service接口
 *
 * @author xianyue
 * @date 2021-01-14
 */
public interface IDdAssTempService {
    /**
     * 查询模板考核，各个学校考核记录
     *
     * @param id 模板考核，各个学校考核记录ID
     * @return 模板考核，各个学校考核记录
     */
    public DdAssTemp selectDdAssTempById(Integer id);

    /**
     * 查询模板考核，各个学校考核记录列表
     *
     * @param ddAssTemp 模板考核，各个学校考核记录
     * @return 模板考核，各个学校考核记录集合
     */
    public List<DdAssTemp> selectDdAssTempList(DdAssTemp ddAssTemp);

    /**
     * 新增模板考核，各个学校考核记录
     *
     * @param ddAssTemp 模板考核，各个学校考核记录
     * @return 结果
     */
    public int insertDdAssTemp(DdAssTemp ddAssTemp);

    /**
     * 修改模板考核，各个学校考核记录
     *
     * @param ddAssTemp 模板考核，各个学校考核记录
     * @return 结果
     */
    public int updateDdAssTemp(DdAssTemp ddAssTemp);

    /**
     * 批量删除模板考核，各个学校考核记录
     *
     * @param ids 需要删除的模板考核，各个学校考核记录ID
     * @return 结果
     */
    public int deleteDdAssTempByIds(Integer[] ids);

    /**
     * 删除模板考核，各个学校考核记录信息
     *
     * @param id 模板考核，各个学校考核记录ID
     * @return 结果
     */
    public int deleteDdAssTempById(Integer id);
}
