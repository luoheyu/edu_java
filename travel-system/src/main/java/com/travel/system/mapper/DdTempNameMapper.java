package com.travel.system.mapper;

import com.travel.system.domain.DdTempName;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 模板名（01）Mapper接口
 *
 * @author xianyue
 * @date 2021-01-11
 */

public interface DdTempNameMapper {
    /**
     * 查询模板名（01）
     *
     * @param id 模板名（01）ID
     * @return 模板名（01）
     */

    public DdTempName selectDdTempNameById(Integer id);


    /**
     * 查询模板名（01）列表
     *
     * @param ddTempName 模板名（01）
     * @return 模板名（01）集合
     */

    public List<DdTempName> selectDdTempNameList(DdTempName ddTempName);


    /**
     * 新增模板名（01）
     *
     * @param ddTempName 模板名（01）
     * @return 结果
     */

    public int insertDdTempName(DdTempName ddTempName);


    /**
     * 修改模板名（01）
     *
     * @param ddTempName 模板名（01）
     * @return 结果
     */

    public int updateDdTempName(DdTempName ddTempName);


    /**
     * 删除模板名（01）
     *
     * @param id 模板名（01）ID
     * @return 结果
     */

    public int deleteDdTempNameById(Integer id);


    /**
     * 批量删除模板名（01）
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */

    public int deleteDdTempNameByIds(Integer[] ids);

    /**
    *获取模板列表
    *
    */
    @Select("select id,temp_name as tempName from dd_temp_name")
    public List<DdTempName> getTempList();

}
