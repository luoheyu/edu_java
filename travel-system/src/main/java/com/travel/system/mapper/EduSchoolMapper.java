package com.travel.system.mapper;


import com.travel.system.domain.EduSchool;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author xianyue
 * @date 2020-12-21
 */

public interface EduSchoolMapper {

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */

    public EduSchool selectEduSchoolById(Integer id);


    /**
     * 查询【请填写功能名称】列表
     *
     * @param eduSchool 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */

    public List<EduSchool> selectEduSchoolList(EduSchool eduSchool);


    /**
     * 新增【请填写功能名称】
     *
     * @param eduSchool 【请填写功能名称】
     * @return 结果
     */

    public int insertEduSchool(EduSchool eduSchool);


    /**
     * 修改【请填写功能名称】
     *
     * @param eduSchool 【请填写功能名称】
     * @return 结果
     */

    public int updateEduSchool(EduSchool eduSchool);


    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */

    public int deleteEduSchoolById(Integer id);


    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */

    public int deleteEduSchoolByIds(Integer[] ids);

    /**
     * 查询所有的初中
     *
     * @param
     * @return 结果
     */
    @Select("select id,school_name as schoolName from edu_school where fid=0")
    public List<EduSchool> getAllMiddle();

    /**
     * 根据id获取学校名称
     *
     * @param id
     * @return 结果
     */
    @Select("select school_name as schoolName from edu_school where id = #{id}")
    public EduSchool getSchoolNameId(Integer id);

    /**
     * 根据fid获取学校列表
     *
     * @param fid
     * @return 结果
     */
    @Select("select id,fid,school_name as schoolName,school_type as schoolType from edu_school where fid = #{fid}")
    public List<EduSchool> getSchooList(Integer fid);

    /**
     * 根据小学id获取父级id
     */
    @Select("select * from edu_school where id = #{id}")
    public EduSchool getSchoolParent(Integer id);

    /**
     * 获取所有学校
     * @return 结果
     */
    @Select("select id,school_name as schoolName from edu_school ")
    public List<EduSchool> getAllSchool();
    /**
    *根据镇id获取下面的学校
    *
    */
    @Select("select id,school_name as schoolName from edu_school where town_id = #{id}")
    List<EduSchool> getSchoolByTownId(Integer id);
}
