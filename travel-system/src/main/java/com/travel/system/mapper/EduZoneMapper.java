package com.travel.system.mapper;

import com.travel.system.domain.EduZone;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 村镇管理Mapper接口
 *
 * @author yu
 * @date 2020-12-23
 */

public interface EduZoneMapper {

    /**
     * 查询村镇管理
     *
     * @param id 村镇管理ID
     * @return 村镇管理
     */

    public EduZone selectEduZoneById(Integer id);


    /**
     * 查询村镇管理列表
     *
     * @param eduZone 村镇管理
     * @return 村镇管理集合
     */

    public List<EduZone> selectEduZoneList(EduZone eduZone);


    /**
     * 新增村镇管理
     *
     * @param eduZone 村镇管理
     * @return 结果
     */

    public int insertEduZone(EduZone eduZone);


    /**
     * 修改村镇管理
     *
     * @param eduZone 村镇管理
     * @return 结果
     */

    public int updateEduZone(EduZone eduZone);


    /**
     * 删除村镇管理
     *
     * @param id 村镇管理ID
     * @return 结果
     */

    public int deleteEduZoneById(Integer id);


    /**
     * 批量删除村镇管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */

    public int deleteEduZoneByIds(Integer[] ids);

    /**
     * 查询所有的镇
     *
     * @param
     * @return 结果
     */
    @Select("select * from edu_zone where fid=0")
    public List<EduZone> getZhen();
    /**
     * 根据id获取村镇名称
     *
     * @param  id
     * @return 结果
     */
    @Select("select name from edu_zone where id = #{id}")
    public EduZone getNameById(Integer id);

    /**
     * 根据fid获取村镇列表
     */
    @Select("select * from edu_zone where fid=#{fid}")
    public List<EduZone> getZoneList(Integer fid);
    /**
    *获取全部村镇
    *
    */
    @Select("select a.* from edu_zone a")
    public List<EduZone> getAllZone();

}

