package com.travel.system.mapper;

import com.travel.system.domain.DdAssessitem;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 反馈内容细项Mapper接口
 *
 * @author xianyue
 * @date 2021-01-18
 */
public interface DdAssessitemMapper {
    /**
     * 查询反馈内容细项
     *
     * @param id 反馈内容细项ID
     * @return 反馈内容细项
     */
    public DdAssessitem selectDdAssessitemById(Integer id);

    /**
     * 查询反馈内容细项列表
     *
     * @param ddAssessitem 反馈内容细项
     * @return 反馈内容细项集合
     */
    public List<DdAssessitem> selectDdAssessitemList(DdAssessitem ddAssessitem);

    /**
     * 新增反馈内容细项
     *
     * @param ddAssessitem 反馈内容细项
     * @return 结果
     */
    public int insertDdAssessitem(DdAssessitem ddAssessitem);

    /**
     * 修改反馈内容细项
     *
     * @param ddAssessitem 反馈内容细项
     * @return 结果
     */
    public int updateDdAssessitem(DdAssessitem ddAssessitem);

    /**
     * 删除反馈内容细项
     *
     * @param id 反馈内容细项ID
     * @return 结果
     */
    public int deleteDdAssessitemById(Integer id);

    /**
     * 批量删除反馈内容细项
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDdAssessitemByIds(Integer[] ids);

    /**
    *根据id获取详情
    *
    */
    @Select("select a.*,a.feedback_imgs as feedbackImgs,a.question_imgs as questionImgs from `dd_assessitem` a where id = #{id}")
    public DdAssessitem getDetail(Integer id);
}
