package com.travel.system.mapper;

import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzStudentStanding;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 6-16周岁台账Mapper接口
 *
 * @author xianyue
 * @date 2021-02-03
 */
public interface TzStudentStandingMapper {
    /**
     * 查询6周岁台账
     *
     * @param id 6周岁台账ID
     * @return 6周岁台账
     */
    public TzStudentStanding selectTzStudentStandingById(Integer id);

    /**
     * 查询6周岁台账列表
     *
     * @param tzStudentStanding 6周岁台账
     * @return 6周岁台账集合
     */
    public List<TzStudentStanding> selectTzStudentStandingList(TzStudentStanding tzStudentStanding);

    /**
     * 新增6周岁台账
     *
     * @param tzStudentStanding 6周岁台账
     * @return 结果
     */
    public int insertTzStudentStanding(TzStudentStanding tzStudentStanding);

    /**
     * 修改6周岁台账
     *
     * @param tzStudentStanding 6周岁台账
     * @return 结果
     */
    public int updateTzStudentStanding(TzStudentStanding tzStudentStanding);

    /**
     * 删除6周岁台账
     *
     * @param id 6周岁台账ID
     * @return 结果
     */
    public int deleteTzStudentStandingById(Integer id);

    /**
     * 批量删除6周岁台账
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTzStudentStandingByIds(Integer[] ids);

    /**
     * 获取统计年份
     */
    @Select("SELECT DISTINCT(total_year),a.*,a.total_year AS totalYear FROM ${tName} a")
    public List<TzStudentStanding> getTotalYear(String tName);

    //创建6-16周岁台账
    @Insert("insert into ${tName}(name,birth,idcard,sex,farmers,town_id,village_id," +
            "address,middle_school,young_school,guardian,phone,record," +
            "edge_people,disability,behind_children,accompanying_children,is_late," +
            "in_school,drop_school,graduation,study_young,study_middle,out_city,out_province,remark,total_year,)values(" +
            "<foreach collection=\"list\" item=\"item\" index=\"index\" separator=\",\">" +
            "(" +
            "#{item.name},#{item.birth},#{item.idcard},#{item.sex},#{item.farmers},#{item.townId},#{item.villageId}," +
            "#{item.address},#{item.middleSchool},#{item.youngSchool},#{item.guardian},#{item.phone},#{item.record}," +
            "#{item.edgePeople},#{item.disability},#{item.behindChildren},#{item.accompanyingChildren},#{item.isLate}," +
            "#{item.inSchool},#{item.dropSchool},#{item.graduation},#{item.studyYoung},#{item.studyMiddle},#{item.outCity}," +
            "#{item.outProvince},#{item.remark},#{item.totalYear}" +
            ")" +
            "</foreach>" +
            ")")
    public void createTotal(@Param("list") List<EduStudent> list, @Param("tName") String tName);
}

