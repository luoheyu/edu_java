package com.travel.system.mapper;

import com.travel.system.domain.EduArticle;

import java.util.List;

/**
 * 政策文件Mapper接口
 *
 * @author yu
 * @date 2021-01-08
 */

public interface EduArticleMapper {
    /**
     * 查询政策文件
     *
     * @param id 政策文件ID
     * @return 政策文件
     */

    public EduArticle selectEduArticleById(Integer id);


    /**
     * 查询政策文件列表
     *
     * @param eduArticle 政策文件
     * @return 政策文件集合
     */

    public List<EduArticle> selectEduArticleList(EduArticle eduArticle);


    /**
     * 新增政策文件
     *
     * @param eduArticle 政策文件
     * @return 结果
     */

    public int insertEduArticle(EduArticle eduArticle);


    /**
     * 修改政策文件
     *
     * @param eduArticle 政策文件
     * @return 结果
     */

    public int updateEduArticle(EduArticle eduArticle);


    /**
     * 删除政策文件
     *
     * @param id 政策文件ID
     * @return 结果
     */

    public int deleteEduArticleById(Integer id);


    /**
     * 批量删除政策文件
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */

    public int deleteEduArticleByIds(Integer[] ids);

}
