package com.travel.system.mapper;

import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzPoorStudentStanding;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 贫困学生台账统计Mapper接口
 *
 * @author xianyue
 * @date 2021-01-27
 */
public interface TzPoorStudentStandingMapper {
    /**
     * 查询贫困学生台账统计
     *
     * @param id 贫困学生台账统计ID
     * @return 贫困学生台账统计
     */
    public TzPoorStudentStanding selectTzPoorStudentStandingById(Integer id);

    /**
     * 查询贫困学生台账统计列表
     *
     * @param tzPoorStudentStanding 贫困学生台账统计
     * @return 贫困学生台账统计集合
     */
    public List<TzPoorStudentStanding> selectTzPoorStudentStandingList(TzPoorStudentStanding tzPoorStudentStanding);

    /**
     * 新增贫困学生台账统计
     *
     * @param tzPoorStudentStanding 贫困学生台账统计
     * @return 结果
     */
    public int insertTzPoorStudentStanding(TzPoorStudentStanding tzPoorStudentStanding);

    /**
     * 修改贫困学生台账统计
     *
     * @param tzPoorStudentStanding 贫困学生台账统计
     * @return 结果
     */
    public int updateTzPoorStudentStanding(TzPoorStudentStanding tzPoorStudentStanding);

    /**
     * 删除贫困学生台账统计
     *
     * @param id 贫困学生台账统计ID
     * @return 结果
     */
    public int deleteTzPoorStudentStandingById(Integer id);

    /**
     * 批量删除贫困学生台账统计
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTzPoorStudentStandingByIds(Integer[] ids);
    /**
     *获取统计的年份
     *
     */
    @Select("SELECT DISTINCT(total_year),a.*,a.total_year AS totalYear FROM `tz_poor_student_standing` a")
    public List<TzPoorStudentStanding> getTotalYear();
    /**
     *插入贫困学生数据
     *
     */
    public void insertStudent(List<EduStudent> list);
}
