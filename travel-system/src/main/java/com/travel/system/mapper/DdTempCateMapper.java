package com.travel.system.mapper;

import com.travel.system.domain.DdTempCate;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 模板分类（02）Mapper接口
 *
 * @author xianyue
 * @date 2021-01-12
 */

public interface DdTempCateMapper {
    /**
     * 查询模板分类（02）
     *
     * @param id 模板分类（02）ID
     * @return 模板分类（02）
     */

    public DdTempCate selectDdTempCateById(Integer id);


    /**
     * 查询模板分类（02）列表
     *
     * @param ddTempCate 模板分类（02）
     * @return 模板分类（02）集合
     */

    public List<DdTempCate> selectDdTempCateList(DdTempCate ddTempCate);


    /**
     * 新增模板分类（02）
     *
     * @param ddTempCate 模板分类（02）
     * @return 结果
     */

    public int insertDdTempCate(DdTempCate ddTempCate);


    /**
     * 修改模板分类（02）
     *
     * @param ddTempCate 模板分类（02）
     * @return 结果
     */

    public int updateDdTempCate(DdTempCate ddTempCate);


    /**
     * 删除模板分类（02）
     *
     * @param id 模板分类（02）ID
     * @return 结果
     */

    public int deleteDdTempCateById(Integer id);


    /**
     * 批量删除模板分类（02）
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */

    public int deleteDdTempCateByIds(Integer[] ids);

    /**
     *根据tempid获取该模板下所有分类
     *
     */
    @Select("select id,name from `dd_temp_cate` where temp_id = #{tempId}")
    public List<DdTempCate> getCatesByTempId(Integer tempId);

}
