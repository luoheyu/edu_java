package com.travel.system.mapper;

import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzSpecialStudentStanding;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 特殊儿童台账Mapper接口
 *
 * @author xianyue
 * @date 2021-01-28
 */
public interface TzSpecialStudentStandingMapper {
    /**
     * 查询特殊儿童台账
     *
     * @param id 特殊儿童台账ID
     * @return 特殊儿童台账
     */
    public TzSpecialStudentStanding selectTzSpecialStudentStandingById(Integer id);

    /**
     * 查询特殊儿童台账列表
     *
     * @param tzSpecialStudentStanding 特殊儿童台账
     * @return 特殊儿童台账集合
     */
    public List<TzSpecialStudentStanding> selectTzSpecialStudentStandingList(TzSpecialStudentStanding tzSpecialStudentStanding);

    /**
     * 新增特殊儿童台账
     *
     * @param tzSpecialStudentStanding 特殊儿童台账
     * @return 结果
     */
    public int insertTzSpecialStudentStanding(TzSpecialStudentStanding tzSpecialStudentStanding);

    /**
     * 修改特殊儿童台账
     *
     * @param tzSpecialStudentStanding 特殊儿童台账
     * @return 结果
     */
    public int updateTzSpecialStudentStanding(TzSpecialStudentStanding tzSpecialStudentStanding);

    /**
     * 删除特殊儿童台账
     *
     * @param id 特殊儿童台账ID
     * @return 结果
     */
    public int deleteTzSpecialStudentStandingById(Integer id);

    /**
     * 批量删除特殊儿童台账
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTzSpecialStudentStandingByIds(Integer[] ids);
    /**
     *获取统计的年份
     *
     */
    @Select("SELECT DISTINCT(total_year),a.*,a.total_year AS totalYear FROM `tz_special_student_standing` a")
    public List<TzSpecialStudentStanding> getTotalYear();
    /**
     *插入贫困学生数据
     *
     */
    public void insertSpecialStudent(List<EduStudent> list);

}
