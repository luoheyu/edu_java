package com.travel.system.mapper;

import com.travel.system.domain.DdTempItem;

import java.util.List;

/**
 * 模板详细内容（03）Mapper接口
 *
 * @author xianyue
 * @date 2021-01-13
 */

public interface DdTempItemMapper {
    /**
     * 查询模板详细内容（03）
     *
     * @param id 模板详细内容（03）ID
     * @return 模板详细内容（03）
     */

    public DdTempItem selectDdTempItemById(Integer id);


    /**
     * 查询模板详细内容（03）列表
     *
     * @param ddTempItem 模板详细内容（03）
     * @return 模板详细内容（03）集合
     */

    public List<DdTempItem> selectDdTempItemList(DdTempItem ddTempItem);


    /**
     * 新增模板详细内容（03）
     *
     * @param ddTempItem 模板详细内容（03）
     * @return 结果
     */

    public int insertDdTempItem(DdTempItem ddTempItem);


    /**
     * 修改模板详细内容（03）
     *
     * @param ddTempItem 模板详细内容（03）
     * @return 结果
     */

    public int updateDdTempItem(DdTempItem ddTempItem);


    /**
     * 删除模板详细内容（03）
     *
     * @param id 模板详细内容（03）ID
     * @return 结果
     */

    public int deleteDdTempItemById(Integer id);


    /**
     * 批量删除模板详细内容（03）
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */

    public int deleteDdTempItemByIds(Integer[] ids);

}
