package com.travel.system.mapper;

import com.travel.system.domain.DdAssTempItem;

import java.util.List;

/**
 * 模板考核，各个学校考核记录之单个条目记录Mapper接口
 *
 * @author xianyue
 * @date 2021-01-14
 */
public interface DdAssTempItemMapper {
    /**
     * 查询模板考核，各个学校考核记录之单个条目记录
     *
     * @param id 模板考核，各个学校考核记录之单个条目记录ID
     * @return 模板考核，各个学校考核记录之单个条目记录
     */
    public DdAssTempItem selectDdAssTempItemById(Integer id);

    /**
     * 查询模板考核，各个学校考核记录之单个条目记录列表
     *
     * @param ddAssTempItem 模板考核，各个学校考核记录之单个条目记录
     * @return 模板考核，各个学校考核记录之单个条目记录集合
     */
    public List<DdAssTempItem> selectDdAssTempItemList(DdAssTempItem ddAssTempItem);

    /**
     * 新增模板考核，各个学校考核记录之单个条目记录
     *
     * @param ddAssTempItem 模板考核，各个学校考核记录之单个条目记录
     * @return 结果
     */
    public int insertDdAssTempItem(DdAssTempItem ddAssTempItem);

    /**
     * 修改模板考核，各个学校考核记录之单个条目记录
     *
     * @param ddAssTempItem 模板考核，各个学校考核记录之单个条目记录
     * @return 结果
     */
    public int updateDdAssTempItem(DdAssTempItem ddAssTempItem);

    /**
     * 删除模板考核，各个学校考核记录之单个条目记录
     *
     * @param id 模板考核，各个学校考核记录之单个条目记录ID
     * @return 结果
     */
    public int deleteDdAssTempItemById(Integer id);

    /**
     * 批量删除模板考核，各个学校考核记录之单个条目记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDdAssTempItemByIds(Integer[] ids);

    /**
    *获取详细信息
    *
    */
    public DdAssTempItem getDetailInfo(Integer id);
}
