package com.travel.system.mapper;

import com.travel.system.domain.DdAssess;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 补充内容表Mapper接口
 *
 * @author xianyue
 * @date 2021-01-18
 */
public interface DdAssessMapper {
    /**
     * 查询补充内容表
     *
     * @param id 补充内容表ID
     * @return 补充内容表
     */
    public DdAssess selectDdAssessById(Integer id);

    /**
     * 查询补充内容表列表
     *
     * @param ddAssess 补充内容表
     * @return 补充内容表集合
     */
    public List<DdAssess> selectDdAssessList(DdAssess ddAssess);

    /**
     * 新增补充内容表
     *
     * @param ddAssess 补充内容表
     * @return 结果
     */
    public int insertDdAssess(DdAssess ddAssess);

    /**
     * 修改补充内容表
     *
     * @param ddAssess 补充内容表
     * @return 结果
     */
    public int updateDdAssess(DdAssess ddAssess);

    /**
     * 删除补充内容表
     *
     * @param id 补充内容表ID
     * @return 结果
     */
    public int deleteDdAssessById(Integer id);

    /**
     * 批量删除补充内容表
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDdAssessByIds(Integer[] ids);

    @Select("select * from `dd_assess` where school_id = #{schoolId} and temp_id = #{tempId}")
    public DdAssess getByIds(@Param("schoolId")Integer schoolId, @Param("tempId") Integer tempId);
}
