package com.travel.system.mapper;

import com.travel.system.domain.DdStudentStanding;
import com.travel.system.domain.EduStudent;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 未入学儿童台账Mapper接口
 *
 * @author xianyue
 * @date 2021-01-25
 */
public interface DdStudentStandingMapper {
    /**
     * 查询未入学儿童台账
     *
     * @param id 未入学儿童台账ID
     * @return 未入学儿童台账
     */
    public DdStudentStanding selectDdStudentStandingById(Integer id);

    /**
     * 查询未入学儿童台账列表
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 未入学儿童台账集合
     */
    public List<DdStudentStanding> selectDdStudentStandingList(DdStudentStanding ddStudentStanding);

    /**
     * 新增未入学儿童台账
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 结果
     */
    public int insertDdStudentStanding(DdStudentStanding ddStudentStanding);

    /**
     * 修改未入学儿童台账
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 结果
     */
    public int updateDdStudentStanding(DdStudentStanding ddStudentStanding);

    /**
     * 删除未入学儿童台账
     *
     * @param id 未入学儿童台账ID
     * @return 结果
     */
    public int deleteDdStudentStandingById(Integer id);

    /**
     * 批量删除未入学儿童台账
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDdStudentStandingByIds(Integer[] ids);
    /**
    *获取统计的年份
    *
    */
    @Select("SELECT DISTINCT(total_year),a.*,a.total_year AS totalYear FROM `dd_student_standing` a")
    public List<DdStudentStanding> getTotalYear();

    /**
    *批量插入数据到未入学儿童统计表
    *
    */
    public void insertStudentToStanding(List<EduStudent> list);
}
