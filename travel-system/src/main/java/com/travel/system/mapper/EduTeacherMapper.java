package com.travel.system.mapper;

import com.travel.system.domain.EduTeacher;

import java.util.List;

/**
 * 教师管理Mapper接口
 *
 * @author xianyue
 * @date 2021-01-06
 */

public interface EduTeacherMapper {
    /**
     * 查询教师管理
     *
     * @param id 教师管理ID
     * @return 教师管理
     */

    public EduTeacher selectEduTeacherById(Long id);


    /**
     * 查询教师管理列表
     *
     * @param eduTeacher 教师管理
     * @return 教师管理集合
     */

    public List<EduTeacher> selectEduTeacherList(EduTeacher eduTeacher);


    /**
     * 新增教师管理
     *
     * @param eduTeacher 教师管理
     * @return 结果
     */

    public int insertEduTeacher(EduTeacher eduTeacher);


    /**
     * 修改教师管理
     *
     * @param eduTeacher 教师管理
     * @return 结果
     */

    public int updateEduTeacher(EduTeacher eduTeacher);


    /**
     * 删除教师管理
     *
     * @param id 教师管理ID
     * @return 结果
     */

    public int deleteEduTeacherById(Long id);


    /**
     * 批量删除教师管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */

    public int deleteEduTeacherByIds(Long[] ids);

}
