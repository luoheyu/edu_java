package com.travel.system.service;

import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzStudentStanding;

import java.util.List;

/**
 * 6周岁台账Service接口
 *
 * @author xianyue
 * @date 2021-02-03
 */

public interface ITzStudentStandingService {
    /**
     * 查询6周岁台账
     *
     * @param id 6周岁台账ID
     * @return 6周岁台账
     */

    public TzStudentStanding selectTzStudentStandingById(Integer id);


    /**
     * 查询6周岁台账列表
     *
     * @param tzStudentStanding 6周岁台账
     * @return 6周岁台账集合
     */

    public List<TzStudentStanding> selectTzStudentStandingList(TzStudentStanding tzStudentStanding);


    /**
     * 新增6周岁台账
     *
     * @param tzStudentStanding 6周岁台账
     * @return 结果
     */

    public int insertTzStudentStanding(TzStudentStanding tzStudentStanding);


    /**
     * 修改6周岁台账
     *
     * @param tzStudentStanding 6周岁台账
     * @return 结果
     */

    public int updateTzStudentStanding(TzStudentStanding tzStudentStanding);


    /**
     * 批量删除6周岁台账
     *
     * @param ids 需要删除的6周岁台账ID
     * @return 结果
     */

    public int deleteTzStudentStandingByIds(Integer[] ids);


    /**
     * 删除6周岁台账信息
     *
     * @param id 6周岁台账ID
     * @return 结果
     */

    public int deleteTzStudentStandingById(Integer id);

    /**
    *获取统计年份
    *
    */
    public List<TzStudentStanding> getTotalYear(String tName);
    /**
    *生成6-16周岁学生台账
    *
    */
    public void createTotal(List<EduStudent> list,String tName);

}
