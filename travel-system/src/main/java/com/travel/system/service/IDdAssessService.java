package com.travel.system.service;

import com.travel.system.domain.DdAssess;

import java.util.List;

/**
 * 补充内容表Service接口
 *
 * @author xianyue
 * @date 2021-01-18
 */
public interface IDdAssessService {
    /**
     * 查询补充内容表
     *
     * @param id 补充内容表ID
     * @return 补充内容表
     */
    public DdAssess selectDdAssessById(Integer id);

    /**
     * 查询补充内容表列表
     *
     * @param ddAssess 补充内容表
     * @return 补充内容表集合
     */
    public List<DdAssess> selectDdAssessList(DdAssess ddAssess);

    /**
     * 新增补充内容表
     *
     * @param ddAssess 补充内容表
     * @return 结果
     */
    public int insertDdAssess(DdAssess ddAssess);

    /**
     * 修改补充内容表
     *
     * @param ddAssess 补充内容表
     * @return 结果
     */
    public int updateDdAssess(DdAssess ddAssess);

    /**
     * 批量删除补充内容表
     *
     * @param ids 需要删除的补充内容表ID
     * @return 结果
     */
    public int deleteDdAssessByIds(Integer[] ids);

    /**
     * 删除补充内容表信息
     *
     * @param id 补充内容表ID
     * @return 结果
     */
    public int deleteDdAssessById(Integer id);

    /**
    *根据schoolid和tempid获取assess表中的id
    *
    */
    public DdAssess getByIds(Integer schoolId, Integer tempId);

}
