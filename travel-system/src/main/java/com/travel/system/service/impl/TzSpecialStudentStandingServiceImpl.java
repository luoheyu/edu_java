package com.travel.system.service.impl;

import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzSpecialStudentStanding;
import com.travel.system.mapper.TzSpecialStudentStandingMapper;
import com.travel.system.service.ITzSpecialStudentStandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 特殊儿童台账Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-28
 */
@Service
public class TzSpecialStudentStandingServiceImpl implements ITzSpecialStudentStandingService {
    @Autowired
    private TzSpecialStudentStandingMapper tzSpecialStudentStandingMapper;

    /**
     * 查询特殊儿童台账
     *
     * @param id 特殊儿童台账ID
     * @return 特殊儿童台账
     */
    @Override
    public TzSpecialStudentStanding selectTzSpecialStudentStandingById(Integer id) {
        return tzSpecialStudentStandingMapper.selectTzSpecialStudentStandingById(id);
    }

    /**
     * 查询特殊儿童台账列表
     *
     * @param tzSpecialStudentStanding 特殊儿童台账
     * @return 特殊儿童台账
     */
    @Override
    public List<TzSpecialStudentStanding> selectTzSpecialStudentStandingList(TzSpecialStudentStanding tzSpecialStudentStanding) {
        return tzSpecialStudentStandingMapper.selectTzSpecialStudentStandingList(tzSpecialStudentStanding);
    }

    /**
     * 新增特殊儿童台账
     *
     * @param tzSpecialStudentStanding 特殊儿童台账
     * @return 结果
     */
    @Override
    public int insertTzSpecialStudentStanding(TzSpecialStudentStanding tzSpecialStudentStanding) {
        return tzSpecialStudentStandingMapper.insertTzSpecialStudentStanding(tzSpecialStudentStanding);
    }

    /**
     * 修改特殊儿童台账
     *
     * @param tzSpecialStudentStanding 特殊儿童台账
     * @return 结果
     */
    @Override
    public int updateTzSpecialStudentStanding(TzSpecialStudentStanding tzSpecialStudentStanding) {
        return tzSpecialStudentStandingMapper.updateTzSpecialStudentStanding(tzSpecialStudentStanding);
    }

    /**
     * 批量删除特殊儿童台账
     *
     * @param ids 需要删除的特殊儿童台账ID
     * @return 结果
     */
    @Override
    public int deleteTzSpecialStudentStandingByIds(Integer[] ids) {
        return tzSpecialStudentStandingMapper.deleteTzSpecialStudentStandingByIds(ids);
    }

    /**
     * 删除特殊儿童台账信息
     *
     * @param id 特殊儿童台账ID
     * @return 结果
     */
    @Override
    public int deleteTzSpecialStudentStandingById(Integer id) {
        return tzSpecialStudentStandingMapper.deleteTzSpecialStudentStandingById(id);
    }
    /**
     *获取统计的年份
     *
     */
    @Override
    public List<TzSpecialStudentStanding> getTotalYear(){
        return tzSpecialStudentStandingMapper.getTotalYear();
    };
    /**
     *插入贫困学生数据
     *
     */
    @Override
    public void insertSpecialStudent(List<EduStudent> list){
        tzSpecialStudentStandingMapper.insertSpecialStudent(list);
    }
}
