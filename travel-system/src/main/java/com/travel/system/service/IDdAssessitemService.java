package com.travel.system.service;

import com.travel.system.domain.DdAssessitem;

import java.util.List;

/**
 * 反馈内容细项Service接口
 *
 * @author xianyue
 * @date 2021-01-18
 */
public interface IDdAssessitemService {
    /**
     * 查询反馈内容细项
     *
     * @param id 反馈内容细项ID
     * @return 反馈内容细项
     */
    public DdAssessitem selectDdAssessitemById(Integer id);

    /**
     * 查询反馈内容细项列表
     *
     * @param ddAssessitem 反馈内容细项
     * @return 反馈内容细项集合
     */
    public List<DdAssessitem> selectDdAssessitemList(DdAssessitem ddAssessitem);

    /**
     * 新增反馈内容细项
     *
     * @param ddAssessitem 反馈内容细项
     * @return 结果
     */
    public int insertDdAssessitem(DdAssessitem ddAssessitem);

    /**
     * 修改反馈内容细项
     *
     * @param ddAssessitem 反馈内容细项
     * @return 结果
     */
    public int updateDdAssessitem(DdAssessitem ddAssessitem);

    /**
     * 批量删除反馈内容细项
     *
     * @param ids 需要删除的反馈内容细项ID
     * @return 结果
     */
    public int deleteDdAssessitemByIds(Integer[] ids);

    /**
     * 删除反馈内容细项信息
     *
     * @param id 反馈内容细项ID
     * @return 结果
     */
    public int deleteDdAssessitemById(Integer id);
    /**
    *根据id获取详请
    *
    */
    public DdAssessitem getDetail(Integer id);
}
