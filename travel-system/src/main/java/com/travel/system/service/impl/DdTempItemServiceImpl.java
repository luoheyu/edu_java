package com.travel.system.service.impl;

import com.travel.system.domain.DdTempItem;
import com.travel.system.mapper.DdTempItemMapper;
import com.travel.system.service.IDdTempItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 模板详细内容（03）Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-13
 */
@Service
public class DdTempItemServiceImpl implements IDdTempItemService {
    @Autowired
    private DdTempItemMapper ddTempItemMapper;

    /**
     * 查询模板详细内容（03）
     *
     * @param id 模板详细内容（03）ID
     * @return 模板详细内容（03）
     */
    @Override
    public DdTempItem selectDdTempItemById(Integer id) {
        return ddTempItemMapper.selectDdTempItemById(id);
    }

    /**
     * 查询模板详细内容（03）列表
     *
     * @param ddTempItem 模板详细内容（03）
     * @return 模板详细内容（03）
     */
    @Override
    public List<DdTempItem> selectDdTempItemList(DdTempItem ddTempItem) {
        return ddTempItemMapper.selectDdTempItemList(ddTempItem);
    }

    /**
     * 新增模板详细内容（03）
     *
     * @param ddTempItem 模板详细内容（03）
     * @return 结果
     */
    @Override
    public int insertDdTempItem(DdTempItem ddTempItem) {
        return ddTempItemMapper.insertDdTempItem(ddTempItem);
    }

    /**
     * 修改模板详细内容（03）
     *
     * @param ddTempItem 模板详细内容（03）
     * @return 结果
     */
    @Override
    public int updateDdTempItem(DdTempItem ddTempItem) {
        return ddTempItemMapper.updateDdTempItem(ddTempItem);
    }

    /**
     * 批量删除模板详细内容（03）
     *
     * @param ids 需要删除的模板详细内容（03）ID
     * @return 结果
     */
    @Override
    public int deleteDdTempItemByIds(Integer[] ids) {
        return ddTempItemMapper.deleteDdTempItemByIds(ids);
    }

    /**
     * 删除模板详细内容（03）信息
     *
     * @param id 模板详细内容（03）ID
     * @return 结果
     */
    @Override
    public int deleteDdTempItemById(Integer id) {
        return ddTempItemMapper.deleteDdTempItemById(id);
    }
}
