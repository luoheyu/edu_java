package com.travel.system.service.impl;

import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzStudentStanding;
import com.travel.system.mapper.TzStudentStandingMapper;
import com.travel.system.service.ITzStudentStandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 6周岁台账Service业务层处理
 *
 * @author xianyue
 * @date 2021-02-03
 */
@Service
public class TzStudentStandingServiceImpl implements ITzStudentStandingService {
    @Autowired
    private TzStudentStandingMapper tzStudentStandingMapper;

    /**
     * 查询6周岁台账
     *
     * @param id 6周岁台账ID
     * @return 6周岁台账
     */
    @Override
    public TzStudentStanding selectTzStudentStandingById(Integer id) {
        return tzStudentStandingMapper.selectTzStudentStandingById(id);
    }

    /**
     * 查询6周岁台账列表
     *
     * @param tzStudentStanding 6周岁台账
     * @return 6周岁台账
     */
    @Override
    public List<TzStudentStanding> selectTzStudentStandingList(TzStudentStanding tzStudentStanding) {
        return tzStudentStandingMapper.selectTzStudentStandingList(tzStudentStanding);
    }

    /**
     * 新增6周岁台账
     *
     * @param tzStudentStanding 6周岁台账
     * @return 结果
     */
    @Override
    public int insertTzStudentStanding(TzStudentStanding tzStudentStanding) {
        return tzStudentStandingMapper.insertTzStudentStanding(tzStudentStanding);
    }

    /**
     * 修改6周岁台账
     *
     * @param tzStudentStanding 6周岁台账
     * @return 结果
     */
    @Override
    public int updateTzStudentStanding(TzStudentStanding tzStudentStanding) {
        return tzStudentStandingMapper.updateTzStudentStanding(tzStudentStanding);
    }

    /**
     * 批量删除6周岁台账
     *
     * @param ids 需要删除的6周岁台账ID
     * @return 结果
     */
    @Override
    public int deleteTzStudentStandingByIds(Integer[] ids) {
        return tzStudentStandingMapper.deleteTzStudentStandingByIds(ids);
    }

    /**
     * 删除6周岁台账信息
     *
     * @param id 6周岁台账ID
     * @return 结果
     */
    @Override
    public int deleteTzStudentStandingById(Integer id) {
        return tzStudentStandingMapper.deleteTzStudentStandingById(id);
    }

    /**
     * 获取统计年份
     */
    @Override
    public List<TzStudentStanding> getTotalYear(String tName) {
        return tzStudentStandingMapper.getTotalYear(tName);

    }
    /**
     *生成6-16周岁学生台账
     *
     */
    @Override
    public void createTotal(List<EduStudent> list, String tName){
        tzStudentStandingMapper.createTotal(list,tName);
    }
}
