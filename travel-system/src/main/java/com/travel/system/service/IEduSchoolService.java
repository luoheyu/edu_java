package com.travel.system.service;

import com.travel.system.domain.DataModel.SchoolTreeSelect;
import com.travel.system.domain.EduSchool;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author xianyue
 * @date 2020-12-21
 */
public interface IEduSchoolService {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public EduSchool selectEduSchoolById(Integer id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param eduSchool 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<EduSchool> selectEduSchoolList(EduSchool eduSchool);

    /**
     * 新增【请填写功能名称】
     *
     * @param eduSchool 【请填写功能名称】
     * @return 结果
     */
    public int insertEduSchool(EduSchool eduSchool);

    /**
     * 修改【请填写功能名称】
     *
     * @param eduSchool 【请填写功能名称】
     * @return 结果
     */
    public int updateEduSchool(EduSchool eduSchool);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteEduSchoolByIds(Integer[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteEduSchoolById(Integer id);

    /**
     * 获取所有的初中
     *
     * @param
     * @return 结果
     */
    public List<EduSchool> getAllMiddle();
    /**
     * 根据id获取学校名称
     *
     * @param  id
     * @return 结果
     */
    public EduSchool getSchoolNameId(Integer id);
    /**
     * 根据fid获取学校
     *
     * @param  fid
     * @return 结果
     */
    public List<EduSchool> getSchoolList(Integer fid);

    /**
     * 根据学校id获取父级id
     * @param id
     * @return
     */
    public EduSchool getSchoolParent(Integer id);
    /**
     * 获取所有学校数据
     * @param
     * @return
     */
    public List<EduSchool> getAllSchool();
    /**
    *根据镇id获取下面的学校
    *
    */
    public List<EduSchool> getSchoolByTownId(Integer id);
    /**
     *构建前段需要的下拉树结构数据
     *
     */
    public List<EduSchool> buildSchoolTree(List<EduSchool> schools);

    public List<SchoolTreeSelect> buildSchoolTreeSelect(List<EduSchool> schools);
}
