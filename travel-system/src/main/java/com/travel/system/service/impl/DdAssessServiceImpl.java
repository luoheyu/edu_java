package com.travel.system.service.impl;

import com.travel.common.utils.DateUtils;
import com.travel.system.domain.DdAssess;
import com.travel.system.mapper.DdAssessMapper;
import com.travel.system.service.IDdAssessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 补充内容表Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-18
 */
@Service
public class DdAssessServiceImpl implements IDdAssessService {
    @Autowired
    private DdAssessMapper ddAssessMapper;

    /**
     * 查询补充内容表
     *
     * @param id 补充内容表ID
     * @return 补充内容表
     */
    @Override
    public DdAssess selectDdAssessById(Integer id) {
        return ddAssessMapper.selectDdAssessById(id);
    }

    /**
     * 查询补充内容表列表
     *
     * @param ddAssess 补充内容表
     * @return 补充内容表
     */
    @Override
    public List<DdAssess> selectDdAssessList(DdAssess ddAssess) {
        return ddAssessMapper.selectDdAssessList(ddAssess);
    }

    /**
     * 新增补充内容表
     *
     * @param ddAssess 补充内容表
     * @return 结果
     */
    @Override
    public int insertDdAssess(DdAssess ddAssess) {
        ddAssess.setCreateTime(DateUtils.getNowDate());
        return ddAssessMapper.insertDdAssess(ddAssess);
    }

    /**
     * 修改补充内容表
     *
     * @param ddAssess 补充内容表
     * @return 结果
     */
    @Override
    public int updateDdAssess(DdAssess ddAssess) {
        return ddAssessMapper.updateDdAssess(ddAssess);
    }

    /**
     * 批量删除补充内容表
     *
     * @param ids 需要删除的补充内容表ID
     * @return 结果
     */
    @Override
    public int deleteDdAssessByIds(Integer[] ids) {
        return ddAssessMapper.deleteDdAssessByIds(ids);
    }

    /**
     * 删除补充内容表信息
     *
     * @param id 补充内容表ID
     * @return 结果
     */
    @Override
    public int deleteDdAssessById(Integer id) {
        return ddAssessMapper.deleteDdAssessById(id);
    }

    @Override
    public DdAssess getByIds(Integer schoolId, Integer tempId) {
        return ddAssessMapper.getByIds(schoolId,tempId);
    }
}
