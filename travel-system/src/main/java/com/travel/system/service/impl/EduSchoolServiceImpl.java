package com.travel.system.service.impl;


import com.travel.common.utils.StringUtils;
import com.travel.system.domain.DataModel.SchoolTreeSelect;
import com.travel.system.domain.EduSchool;
import com.travel.system.mapper.EduSchoolMapper;
import com.travel.system.service.IEduSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author xianyue
 * @date 2020-12-21
 */

@Service

public class EduSchoolServiceImpl implements IEduSchoolService {

    @Autowired
    private EduSchoolMapper eduSchoolMapper;


    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */

    @Override
    public EduSchool selectEduSchoolById(Integer id) {

        return eduSchoolMapper.selectEduSchoolById(id);

    }


    /**
     * 查询【请填写功能名称】列表
     *
     * @param eduSchool 【请填写功能名称】
     * @return 【请填写功能名称】
     */

    @Override
    public List<EduSchool> selectEduSchoolList(EduSchool eduSchool) {

        return eduSchoolMapper.selectEduSchoolList(eduSchool);

    }


    /**
     * 新增【请填写功能名称】
     *
     * @param eduSchool 【请填写功能名称】
     * @return 结果
     */

    @Override
    public int insertEduSchool(EduSchool eduSchool) {

        return eduSchoolMapper.insertEduSchool(eduSchool);

    }


    /**
     * 修改【请填写功能名称】
     *
     * @param eduSchool 【请填写功能名称】
     * @return 结果
     */

    @Override
    public int updateEduSchool(EduSchool eduSchool) {

        return eduSchoolMapper.updateEduSchool(eduSchool);

    }


    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteEduSchoolByIds(Integer[] ids) {
        return eduSchoolMapper.deleteEduSchoolByIds(ids);
    }


    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteEduSchoolById(Integer id) {
        return eduSchoolMapper.deleteEduSchoolById(id);

    }

    @Override
    public List<EduSchool> getAllMiddle() {
        return eduSchoolMapper.getAllMiddle();
    }

    /**
     * 根据id获取学校名称
     *
     * @param id
     * @return 结果
     */
    @Override
    public EduSchool getSchoolNameId(Integer id) {
        return eduSchoolMapper.getSchoolNameId(id);
    }

    /**
     * 根据fid获取学校列表
     *
     * @param fid
     * @return 结果
     */
    @Override
    public List<EduSchool> getSchoolList(Integer fid) {
        return eduSchoolMapper.getSchooList(fid);
    }

    @Override
    public EduSchool getSchoolParent(Integer id) {
        return eduSchoolMapper.getSchoolParent(id);
    }

    /**
     * 获取所有学校数据
     */
    @Override
    public List<EduSchool> getAllSchool() {
        return eduSchoolMapper.getAllSchool();
    }

    @Override
    public List<EduSchool> getSchoolByTownId(Integer id) {
        return eduSchoolMapper.getSchoolByTownId(id);
    }

    /**
     * 构建前段需要的下拉树结构数据
     */
    @Override
    public List<SchoolTreeSelect> buildSchoolTreeSelect(List<EduSchool> schools) {
        List<EduSchool> schoolTree = buildSchoolTree(schools);
        return schoolTree.stream().map(SchoolTreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 构建前段需要的下拉树结构数据
     */
    @Override
    public List<EduSchool> buildSchoolTree(List<EduSchool> schools) {
        List<EduSchool> returnList = new ArrayList<EduSchool>();
        List<Integer> tempList = new ArrayList<Integer>();
        for (EduSchool school : schools) {
            tempList.add(school.getId());
        }
        for (Iterator<EduSchool> iterator = schools.iterator(); iterator.hasNext(); ) {
            EduSchool eduSchool = (EduSchool) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(eduSchool.getFid())) {
                //递归列表
                recursionFn(schools, eduSchool);
                returnList.add(eduSchool);
            }
        }
        if (returnList.isEmpty()) {
            returnList = schools;
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<EduSchool> list, EduSchool t) {
        // 得到子节点列表
        List<EduSchool> childList = getChildList(list, t);
        t.setChildren(childList);
        for (EduSchool tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<EduSchool> getChildList(List<EduSchool> list, EduSchool t) {
        List<EduSchool> tlist = new ArrayList<EduSchool>();
        Iterator<EduSchool> it = list.iterator();
        while (it.hasNext()) {
            EduSchool n = (EduSchool) it.next();
            if (StringUtils.isNotNull(n.getFid()) && n.getFid().longValue() == t.getId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<EduSchool> list, EduSchool t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }


}
