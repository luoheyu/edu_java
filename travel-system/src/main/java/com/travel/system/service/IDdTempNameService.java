package com.travel.system.service;

import com.travel.system.domain.DdTempName;

import java.util.List;

/**
 * 模板名（01）Service接口
 *
 * @author xianyue
 * @date 2021-01-11
 */

public interface IDdTempNameService {
    /**
     * 查询模板名（01）
     *
     * @param id 模板名（01）ID
     * @return 模板名（01）
     */

    public DdTempName selectDdTempNameById(Integer id);


    /**
     * 查询模板名（01）列表
     *
     * @param ddTempName 模板名（01）
     * @return 模板名（01）集合
     */

    public List<DdTempName> selectDdTempNameList(DdTempName ddTempName);


    /**
     * 新增模板名（01）
     *
     * @param ddTempName 模板名（01）
     * @return 结果
     */

    public int insertDdTempName(DdTempName ddTempName);


    /**
     * 修改模板名（01）
     *
     * @param ddTempName 模板名（01）
     * @return 结果
     */

    public int updateDdTempName(DdTempName ddTempName);


    /**
     * 批量删除模板名（01）
     *
     * @param ids 需要删除的模板名（01）ID
     * @return 结果
     */

    public int deleteDdTempNameByIds(Integer[] ids);


    /**
     * 删除模板名（01）信息
     *
     * @param id 模板名（01）ID
     * @return 结果
     */

    public int deleteDdTempNameById(Integer id);
    
    /**
    *获取所有模板列表
    *
    */
    public List<DdTempName> getTempList();
}
