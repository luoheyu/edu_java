package com.travel.system.service.impl;

import com.travel.system.domain.DdStudentStanding;
import com.travel.system.domain.EduStudent;
import com.travel.system.mapper.DdStudentStandingMapper;
import com.travel.system.service.IDdStudentStandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 未入学儿童台账Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-25
 */
@Service
public class DdStudentStandingServiceImpl implements IDdStudentStandingService {
    @Autowired
    private DdStudentStandingMapper ddStudentStandingMapper;

    /**
     * 查询未入学儿童台账
     *
     * @param id 未入学儿童台账ID
     * @return 未入学儿童台账
     */
    @Override
    public DdStudentStanding selectDdStudentStandingById(Integer id) {
        return ddStudentStandingMapper.selectDdStudentStandingById(id);
    }

    /**
     * 查询未入学儿童台账列表
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 未入学儿童台账
     */
    @Override
    public List<DdStudentStanding> selectDdStudentStandingList(DdStudentStanding ddStudentStanding) {
        return ddStudentStandingMapper.selectDdStudentStandingList(ddStudentStanding);
    }

    /**
     * 新增未入学儿童台账
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 结果
     */
    @Override
    public int insertDdStudentStanding(DdStudentStanding ddStudentStanding) {
        return ddStudentStandingMapper.insertDdStudentStanding(ddStudentStanding);
    }

    /**
     * 修改未入学儿童台账
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 结果
     */
    @Override
    public int updateDdStudentStanding(DdStudentStanding ddStudentStanding) {
        return ddStudentStandingMapper.updateDdStudentStanding(ddStudentStanding);
    }

    /**
     * 批量删除未入学儿童台账
     *
     * @param ids 需要删除的未入学儿童台账ID
     * @return 结果
     */
    @Override
    public int deleteDdStudentStandingByIds(Integer[] ids) {
        return ddStudentStandingMapper.deleteDdStudentStandingByIds(ids);
    }

    /**
     * 删除未入学儿童台账信息
     *
     * @param id 未入学儿童台账ID
     * @return 结果
     */
    @Override
    public int deleteDdStudentStandingById(Integer id) {
        return ddStudentStandingMapper.deleteDdStudentStandingById(id);
    }
    /**
     *获取统计的年份
     *
     */
    @Override
    public List<DdStudentStanding> getTotalYear(){
        return ddStudentStandingMapper.getTotalYear();
    };
    /**
    *批量插入学生信息
    *
    */
    @Override
    public void insertStudentToStanding(List<EduStudent> list){
        ddStudentStandingMapper.insertStudentToStanding(list);
    }
}
