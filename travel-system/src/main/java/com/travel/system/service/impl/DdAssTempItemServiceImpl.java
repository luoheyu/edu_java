package com.travel.system.service.impl;

import com.travel.common.utils.DateUtils;
import com.travel.system.domain.DdAssTempItem;
import com.travel.system.mapper.DdAssTempItemMapper;
import com.travel.system.service.IDdAssTempItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 模板考核，各个学校考核记录之单个条目记录Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-14
 */
@Service
public class DdAssTempItemServiceImpl implements IDdAssTempItemService {
    @Autowired
    private DdAssTempItemMapper ddAssTempItemMapper;

    /**
     * 查询模板考核，各个学校考核记录之单个条目记录
     *
     * @param id 模板考核，各个学校考核记录之单个条目记录ID
     * @return 模板考核，各个学校考核记录之单个条目记录
     */
    @Override
    public DdAssTempItem selectDdAssTempItemById(Integer id) {
        return ddAssTempItemMapper.selectDdAssTempItemById(id);
    }

    /**
     * 查询模板考核，各个学校考核记录之单个条目记录列表
     *
     * @param ddAssTempItem 模板考核，各个学校考核记录之单个条目记录
     * @return 模板考核，各个学校考核记录之单个条目记录
     */
    @Override
    public List<DdAssTempItem> selectDdAssTempItemList(DdAssTempItem ddAssTempItem) {
        return ddAssTempItemMapper.selectDdAssTempItemList(ddAssTempItem);
    }

    /**
     * 新增模板考核，各个学校考核记录之单个条目记录
     *
     * @param ddAssTempItem 模板考核，各个学校考核记录之单个条目记录
     * @return 结果
     */
    @Override
    public int insertDdAssTempItem(DdAssTempItem ddAssTempItem) {
        ddAssTempItem.setCreateTime(DateUtils.getNowDate());
        return ddAssTempItemMapper.insertDdAssTempItem(ddAssTempItem);
    }

    /**
     * 修改模板考核，各个学校考核记录之单个条目记录
     *
     * @param ddAssTempItem 模板考核，各个学校考核记录之单个条目记录
     * @return 结果
     */
    @Override
    public int updateDdAssTempItem(DdAssTempItem ddAssTempItem) {
        return ddAssTempItemMapper.updateDdAssTempItem(ddAssTempItem);
    }

    /**
     * 批量删除模板考核，各个学校考核记录之单个条目记录
     *
     * @param ids 需要删除的模板考核，各个学校考核记录之单个条目记录ID
     * @return 结果
     */
    @Override
    public int deleteDdAssTempItemByIds(Integer[] ids) {
        return ddAssTempItemMapper.deleteDdAssTempItemByIds(ids);
    }

    /**
     * 删除模板考核，各个学校考核记录之单个条目记录信息
     *
     * @param id 模板考核，各个学校考核记录之单个条目记录ID
     * @return 结果
     */
    @Override
    public int deleteDdAssTempItemById(Integer id) {
        return ddAssTempItemMapper.deleteDdAssTempItemById(id);
    }

    /**
    *获取详细内容
    *
    */
    @Override
    public DdAssTempItem getDetailInfo(Integer id) {
        return ddAssTempItemMapper.getDetailInfo(id);
    }
}
