package com.travel.system.service.impl;

import com.travel.system.domain.TzCitytotal;
import com.travel.system.mapper.TzCitytotalMapper;
import com.travel.system.service.ITzCitytotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 适龄儿童统计（市）Service业务层处理
 *
 * @author xianyue
 * @date 2021-02-05
 */
@Service
public class TzCitytotalServiceImpl implements ITzCitytotalService {
    @Autowired
    private TzCitytotalMapper tzCitytotalMapper;

    /**
     * 查询适龄儿童统计（市）
     *
     * @param id 适龄儿童统计（市）ID
     * @return 适龄儿童统计（市）
     */
    @Override
    public TzCitytotal selectTzCitytotalById(Integer id) {
        return tzCitytotalMapper.selectTzCitytotalById(id);
    }

    /**
     * 查询适龄儿童统计（市）列表
     *
     * @param tzCitytotal 适龄儿童统计（市）
     * @return 适龄儿童统计（市）
     */
    @Override
    public List<TzCitytotal> selectTzCitytotalList(TzCitytotal tzCitytotal) {
        return tzCitytotalMapper.selectTzCitytotalList(tzCitytotal);
    }

    /**
     * 新增适龄儿童统计（市）
     *
     * @param tzCitytotal 适龄儿童统计（市）
     * @return 结果
     */
    @Override
    public int insertTzCitytotal(TzCitytotal tzCitytotal) {
        return tzCitytotalMapper.insertTzCitytotal(tzCitytotal);
    }

    /**
     * 修改适龄儿童统计（市）
     *
     * @param tzCitytotal 适龄儿童统计（市）
     * @return 结果
     */
    @Override
    public int updateTzCitytotal(TzCitytotal tzCitytotal) {
        return tzCitytotalMapper.updateTzCitytotal(tzCitytotal);
    }

    /**
     * 批量删除适龄儿童统计（市）
     *
     * @param ids 需要删除的适龄儿童统计（市）ID
     * @return 结果
     */
    @Override
    public int deleteTzCitytotalByIds(Integer[] ids) {
        return tzCitytotalMapper.deleteTzCitytotalByIds(ids);
    }

    /**
     * 删除适龄儿童统计（市）信息
     *
     * @param id 适龄儿童统计（市）ID
     * @return 结果
     */
    @Override
    public int deleteTzCitytotalById(Integer id) {
        return tzCitytotalMapper.deleteTzCitytotalById(id);
    }
}
