package com.travel.system.service;

import com.travel.system.domain.EduSchool;
import com.travel.system.domain.EduZone;

import java.util.List;


/**
 * 村镇管理Service接口
 *
 * @author yu
 * @date 2020-12-23
 */

public interface IEduZoneService {

    /**
     * 查询村镇管理
     *
     * @param id 村镇管理ID
     * @return 村镇管理
     */

    public EduZone selectEduZoneById(Integer id);


    /**
     * 查询村镇管理列表
     *
     * @param eduZone 村镇管理
     * @return 村镇管理集合
     */

    public List<EduZone> selectEduZoneList(EduZone eduZone);


    /**
     * 新增村镇管理
     *
     * @param eduZone 村镇管理
     * @return 结果
     */

    public int insertEduZone(EduZone eduZone);


    /**
     * 修改村镇管理
     *
     * @param eduZone 村镇管理
     * @return 结果
     */

    public int updateEduZone(EduZone eduZone);


    /**
     * 批量删除村镇管理
     *
     * @param ids 需要删除的村镇管理ID
     * @return 结果
     */

    public int deleteEduZoneByIds(Integer[] ids);


    /**
     * 删除村镇管理信息
     *
     * @param id 村镇管理ID
     * @return 结果
     */

    public int deleteEduZoneById(Integer id);

    /**
     * 获取所有镇
     *
     * @param
     * @return 结果
     */
    public List<EduZone> getZhen();

    /**
     *  根据id获取村镇的名称
     *
     * @param  id
     * @return 结果
     */
    public EduZone getNameById(Integer id);


    public EduSchool getEsInfo(Integer middleSchoolId);
    /**
     * 根据fid获取村镇列表
     */
    public List<EduZone> getZoneList(Integer fid);

    /**
    *获取全部村镇
    *
    */
    public List<EduZone> getAllZone();

}

