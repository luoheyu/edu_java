package com.travel.system.service.impl;

import com.travel.system.domain.DdTempCate;
import com.travel.system.mapper.DdTempCateMapper;
import com.travel.system.service.IDdTempCateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 模板分类（02）Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-12
 */
@Service
public class DdTempCateServiceImpl implements IDdTempCateService {
    @Autowired
    private DdTempCateMapper ddTempCateMapper;

    /**
     * 查询模板分类（02）
     *
     * @param id 模板分类（02）ID
     * @return 模板分类（02）
     */
    @Override
    public DdTempCate selectDdTempCateById(Integer id) {
        return ddTempCateMapper.selectDdTempCateById(id);
    }

    /**
     * 查询模板分类（02）列表
     *
     * @param ddTempCate 模板分类（02）
     * @return 模板分类（02）
     */
    @Override
    public List<DdTempCate> selectDdTempCateList(DdTempCate ddTempCate) {
        return ddTempCateMapper.selectDdTempCateList(ddTempCate);
    }

    /**
     * 新增模板分类（02）
     *
     * @param ddTempCate 模板分类（02）
     * @return 结果
     */
    @Override
    public int insertDdTempCate(DdTempCate ddTempCate) {
        return ddTempCateMapper.insertDdTempCate(ddTempCate);
    }

    /**
     * 修改模板分类（02）
     *
     * @param ddTempCate 模板分类（02）
     * @return 结果
     */
    @Override
    public int updateDdTempCate(DdTempCate ddTempCate) {
        return ddTempCateMapper.updateDdTempCate(ddTempCate);
    }

    /**
     * 批量删除模板分类（02）
     *
     * @param ids 需要删除的模板分类（02）ID
     * @return 结果
     */
    @Override
    public int deleteDdTempCateByIds(Integer[] ids) {
        return ddTempCateMapper.deleteDdTempCateByIds(ids);
    }

    /**
     * 删除模板分类（02）信息
     *
     * @param id 模板分类（02）ID
     * @return 结果
     */
    @Override
    public int deleteDdTempCateById(Integer id) {
        return ddTempCateMapper.deleteDdTempCateById(id);
    }

    /**
     * 根据tempid获取该模板下所有分类
     */
    @Override
    public List<DdTempCate> getCatesByTempId(Integer tempId) {
        return ddTempCateMapper.getCatesByTempId(tempId);
    }
}
