package com.travel.system.service;

import com.travel.system.domain.DdTempCate;

import java.util.List;

/**
 * 模板分类（02）Service接口
 *
 * @author xianyue
 * @date 2021-01-12
 */

public interface IDdTempCateService {
    /**
     * 查询模板分类（02）
     *
     * @param id 模板分类（02）ID
     * @return 模板分类（02）
     */

    public DdTempCate selectDdTempCateById(Integer id);


    /**
     * 查询模板分类（02）列表
     *
     * @param ddTempCate 模板分类（02）
     * @return 模板分类（02）集合
     */

    public List<DdTempCate> selectDdTempCateList(DdTempCate ddTempCate);


    /**
     * 新增模板分类（02）
     *
     * @param ddTempCate 模板分类（02）
     * @return 结果
     */

    public int insertDdTempCate(DdTempCate ddTempCate);


    /**
     * 修改模板分类（02）
     *
     * @param ddTempCate 模板分类（02）
     * @return 结果
     */

    public int updateDdTempCate(DdTempCate ddTempCate);


    /**
     * 批量删除模板分类（02）
     *
     * @param ids 需要删除的模板分类（02）ID
     * @return 结果
     */

    public int deleteDdTempCateByIds(Integer[] ids);


    /**
     * 删除模板分类（02）信息
     *
     * @param id 模板分类（02）ID
     * @return 结果
     */

    public int deleteDdTempCateById(Integer id);

    /**
    *根据tempid获取该模板下所有分类
    *
    */
    public List<DdTempCate> getCatesByTempId(Integer tempId);

}
