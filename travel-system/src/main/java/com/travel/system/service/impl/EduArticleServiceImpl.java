package com.travel.system.service.impl;

import com.travel.common.utils.DateUtils;
import com.travel.system.domain.EduArticle;
import com.travel.system.mapper.EduArticleMapper;
import com.travel.system.service.IEduArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 政策文件Service业务层处理
 *
 * @author yu
 * @date 2021-01-08
 */
@Service
public class EduArticleServiceImpl implements IEduArticleService {
    @Autowired
    private EduArticleMapper eduArticleMapper;

    /**
     * 查询政策文件
     *
     * @param id 政策文件ID
     * @return 政策文件
     */
    @Override
    public EduArticle selectEduArticleById(Integer id) {
        return eduArticleMapper.selectEduArticleById(id);
    }

    /**
     * 查询政策文件列表
     *
     * @param eduArticle 政策文件
     * @return 政策文件
     */
    @Override
    public List<EduArticle> selectEduArticleList(EduArticle eduArticle) {
        return eduArticleMapper.selectEduArticleList(eduArticle);
    }

    /**
     * 新增政策文件
     *
     * @param eduArticle 政策文件
     * @return 结果
     */
    @Override
    public int insertEduArticle(EduArticle eduArticle) {
        eduArticle.setCreateTime(DateUtils.getNowDate());
        return eduArticleMapper.insertEduArticle(eduArticle);
    }

    /**
     * 修改政策文件
     *
     * @param eduArticle 政策文件
     * @return 结果
     */
    @Override
    public int updateEduArticle(EduArticle eduArticle) {
        return eduArticleMapper.updateEduArticle(eduArticle);
    }

    /**
     * 批量删除政策文件
     *
     * @param ids 需要删除的政策文件ID
     * @return 结果
     */
    @Override
    public int deleteEduArticleByIds(Integer[] ids) {
        return eduArticleMapper.deleteEduArticleByIds(ids);
    }

    /**
     * 删除政策文件信息
     *
     * @param id 政策文件ID
     * @return 结果
     */
    @Override
    public int deleteEduArticleById(Integer id) {
        return eduArticleMapper.deleteEduArticleById(id);
    }
}
