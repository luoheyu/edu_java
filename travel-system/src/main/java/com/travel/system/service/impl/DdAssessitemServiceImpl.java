package com.travel.system.service.impl;

import com.travel.system.domain.DdAssessitem;
import com.travel.system.mapper.DdAssessitemMapper;
import com.travel.system.service.IDdAssessitemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 反馈内容细项Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-18
 */
@Service
public class DdAssessitemServiceImpl implements IDdAssessitemService {
    @Autowired
    private DdAssessitemMapper ddAssessitemMapper;

    /**
     * 查询反馈内容细项
     *
     * @param id 反馈内容细项ID
     * @return 反馈内容细项
     */
    @Override
    public DdAssessitem selectDdAssessitemById(Integer id) {
        return ddAssessitemMapper.selectDdAssessitemById(id);
    }

    /**
     * 查询反馈内容细项列表
     *
     * @param ddAssessitem 反馈内容细项
     * @return 反馈内容细项
     */
    @Override
    public List<DdAssessitem> selectDdAssessitemList(DdAssessitem ddAssessitem) {
        return ddAssessitemMapper.selectDdAssessitemList(ddAssessitem);
    }

    /**
     * 新增反馈内容细项
     *
     * @param ddAssessitem 反馈内容细项
     * @return 结果
     */
    @Override
    public int insertDdAssessitem(DdAssessitem ddAssessitem) {
        return ddAssessitemMapper.insertDdAssessitem(ddAssessitem);
    }

    /**
     * 修改反馈内容细项
     *
     * @param ddAssessitem 反馈内容细项
     * @return 结果
     */
    @Override
    public int updateDdAssessitem(DdAssessitem ddAssessitem) {
        return ddAssessitemMapper.updateDdAssessitem(ddAssessitem);
    }

    /**
     * 批量删除反馈内容细项
     *
     * @param ids 需要删除的反馈内容细项ID
     * @return 结果
     */
    @Override
    public int deleteDdAssessitemByIds(Integer[] ids) {
        return ddAssessitemMapper.deleteDdAssessitemByIds(ids);
    }

    /**
     * 删除反馈内容细项信息
     *
     * @param id 反馈内容细项ID
     * @return 结果
     */
    @Override
    public int deleteDdAssessitemById(Integer id) {
        return ddAssessitemMapper.deleteDdAssessitemById(id);
    }
    /**
    *根据id获取详情
    *
    */
    @Override
    public DdAssessitem getDetail(Integer id) {
        return ddAssessitemMapper.getDetail(id);
    }
}
