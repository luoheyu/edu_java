package com.travel.system.service;

import com.travel.system.domain.EduStudent;

import java.util.Date;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author xianyue
 * @date 2020-12-29
 */
public interface IEduStudentService {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public EduStudent selectEduStudentById(Integer id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param eduStudent 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<EduStudent> selectEduStudentList(EduStudent eduStudent);

    /**
     * 新增【请填写功能名称】
     *
     * @param eduStudent 【请填写功能名称】
     * @return 结果
     */
    public int insertEduStudent(EduStudent eduStudent);

    /**
     * 修改【请填写功能名称】
     *
     * @param eduStudent 【请填写功能名称】
     * @return 结果
     */
    public int updateEduStudent(EduStudent eduStudent);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteEduStudentByIds(Integer[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteEduStudentById(Integer id);

    /**
     * 根据起始日期获取学生（未入学儿童）
     */
    public List<EduStudent> getStudentByDate(Date d1, Date d2);

    /**
     * 根据起始日期获取学生（贫困学生）
     */
    public List<EduStudent> getPoorStudent(Date d1, Date d2);

    /**
     * 根据起始日期获取学生（特殊儿童）
     */
    public List<EduStudent> getSpecialStudent(Date d1, Date d2);

    /**
     * 根据日期和镇id获取学生列表
     */
    public List<EduStudent> getStudentByDateAndZone(Date d1, Date d2, Integer zoneId);

    /**
     * 建档立卡户数统计
     */
    public Integer statisticalTotal(Date d1, Date d2, Integer zoneId);
}
