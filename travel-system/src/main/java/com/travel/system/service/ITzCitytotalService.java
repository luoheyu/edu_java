package com.travel.system.service;

import com.travel.system.domain.TzCitytotal;

import java.util.List;

/**
 * 适龄儿童统计（市）Service接口
 *
 * @author xianyue
 * @date 2021-02-05
 */
public interface ITzCitytotalService {
    /**
     * 查询适龄儿童统计（市）
     *
     * @param id 适龄儿童统计（市）ID
     * @return 适龄儿童统计（市）
     */
    public TzCitytotal selectTzCitytotalById(Integer id);

    /**
     * 查询适龄儿童统计（市）列表
     *
     * @param tzCitytotal 适龄儿童统计（市）
     * @return 适龄儿童统计（市）集合
     */
    public List<TzCitytotal> selectTzCitytotalList(TzCitytotal tzCitytotal);

    /**
     * 新增适龄儿童统计（市）
     *
     * @param tzCitytotal 适龄儿童统计（市）
     * @return 结果
     */
    public int insertTzCitytotal(TzCitytotal tzCitytotal);

    /**
     * 修改适龄儿童统计（市）
     *
     * @param tzCitytotal 适龄儿童统计（市）
     * @return 结果
     */
    public int updateTzCitytotal(TzCitytotal tzCitytotal);

    /**
     * 批量删除适龄儿童统计（市）
     *
     * @param ids 需要删除的适龄儿童统计（市）ID
     * @return 结果
     */
    public int deleteTzCitytotalByIds(Integer[] ids);

    /**
     * 删除适龄儿童统计（市）信息
     *
     * @param id 适龄儿童统计（市）ID
     * @return 结果
     */
    public int deleteTzCitytotalById(Integer id);
}
