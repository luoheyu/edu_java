package com.travel.system.service.impl;

import com.travel.common.utils.DateUtils;
import com.travel.system.IDdAssTempService;
import com.travel.system.domain.DdAssTemp;
import com.travel.system.mapper.DdAssTempMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 模板考核，各个学校考核记录Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-14
 */
@Service
public class DdAssTempServiceImpl implements IDdAssTempService {
    @Autowired
    private DdAssTempMapper ddAssTempMapper;

    /**
     * 查询模板考核，各个学校考核记录
     *
     * @param id 模板考核，各个学校考核记录ID
     * @return 模板考核，各个学校考核记录
     */
    @Override
    public DdAssTemp selectDdAssTempById(Integer id) {
        return ddAssTempMapper.selectDdAssTempById(id);
    }

    /**
     * 查询模板考核，各个学校考核记录列表
     *
     * @param ddAssTemp 模板考核，各个学校考核记录
     * @return 模板考核，各个学校考核记录
     */
    @Override
    public List<DdAssTemp> selectDdAssTempList(DdAssTemp ddAssTemp) {
        return ddAssTempMapper.selectDdAssTempList(ddAssTemp);
    }

    /**
     * 新增模板考核，各个学校考核记录
     *
     * @param ddAssTemp 模板考核，各个学校考核记录
     * @return 结果
     */
    @Override
    public int insertDdAssTemp(DdAssTemp ddAssTemp) {
        ddAssTemp.setCreateTime(DateUtils.getNowDate());
        return ddAssTempMapper.insertDdAssTemp(ddAssTemp);
    }

    /**
     * 修改模板考核，各个学校考核记录
     *
     * @param ddAssTemp 模板考核，各个学校考核记录
     * @return 结果
     */
    @Override
    public int updateDdAssTemp(DdAssTemp ddAssTemp) {
        return ddAssTempMapper.updateDdAssTemp(ddAssTemp);
    }

    /**
     * 批量删除模板考核，各个学校考核记录
     *
     * @param ids 需要删除的模板考核，各个学校考核记录ID
     * @return 结果
     */
    @Override
    public int deleteDdAssTempByIds(Integer[] ids) {
        return ddAssTempMapper.deleteDdAssTempByIds(ids);
    }

    /**
     * 删除模板考核，各个学校考核记录信息
     *
     * @param id 模板考核，各个学校考核记录ID
     * @return 结果
     */
    @Override
    public int deleteDdAssTempById(Integer id) {
        return ddAssTempMapper.deleteDdAssTempById(id);
    }
}
