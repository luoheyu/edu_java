package com.travel.system.service.impl;

import com.travel.common.utils.DateUtils;
import com.travel.system.domain.DdTempName;
import com.travel.system.mapper.DdTempNameMapper;
import com.travel.system.service.IDdTempNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 模板名（01）Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-11
 */
@Service
public class DdTempNameServiceImpl implements IDdTempNameService {
    @Autowired
    private DdTempNameMapper ddTempNameMapper;

    /**
     * 查询模板名（01）
     *
     * @param id 模板名（01）ID
     * @return 模板名（01）
     */
    @Override
    public DdTempName selectDdTempNameById(Integer id) {
        return ddTempNameMapper.selectDdTempNameById(id);
    }

    /**
     * 查询模板名（01）列表
     *
     * @param ddTempName 模板名（01）
     * @return 模板名（01）
     */
    @Override
    public List<DdTempName> selectDdTempNameList(DdTempName ddTempName) {
        return ddTempNameMapper.selectDdTempNameList(ddTempName);
    }

    /**
     * 新增模板名（01）
     *
     * @param ddTempName 模板名（01）
     * @return 结果
     */
    @Override
    public int insertDdTempName(DdTempName ddTempName) {
        ddTempName.setCreateTime(DateUtils.getNowDate());
        return ddTempNameMapper.insertDdTempName(ddTempName);
    }

    /**
     * 修改模板名（01）
     *
     * @param ddTempName 模板名（01）
     * @return 结果
     */
    @Override
    public int updateDdTempName(DdTempName ddTempName) {
        return ddTempNameMapper.updateDdTempName(ddTempName);
    }

    /**
     * 批量删除模板名（01）
     *
     * @param ids 需要删除的模板名（01）ID
     * @return 结果
     */
    @Override
    public int deleteDdTempNameByIds(Integer[] ids) {
        return ddTempNameMapper.deleteDdTempNameByIds(ids);
    }

    /**
     * 删除模板名（01）信息
     *
     * @param id 模板名（01）ID
     * @return 结果
     */
    @Override
    public int deleteDdTempNameById(Integer id) {
        return ddTempNameMapper.deleteDdTempNameById(id);
    }

    @Override
    public List<DdTempName> getTempList() {
        return ddTempNameMapper.getTempList();
    }
}
