package com.travel.system.service.impl;

import com.travel.common.utils.DateUtils;
import com.travel.system.domain.EduStudent;
import com.travel.system.mapper.EduStudentMapper;
import com.travel.system.service.IEduStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author xianyue
 * @date 2020-12-29
 */
@Service
public class EduStudentServiceImpl implements IEduStudentService {
    @Autowired
    private EduStudentMapper eduStudentMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public EduStudent selectEduStudentById(Integer id) {
        return eduStudentMapper.selectEduStudentById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param eduStudent 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<EduStudent> selectEduStudentList(EduStudent eduStudent) {
        return eduStudentMapper.selectEduStudentList(eduStudent);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param eduStudent 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertEduStudent(EduStudent eduStudent) {
        eduStudent.setCreateTime(DateUtils.getNowDate());
        return eduStudentMapper.insertEduStudent(eduStudent);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param eduStudent 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateEduStudent(EduStudent eduStudent) {
        return eduStudentMapper.updateEduStudent(eduStudent);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteEduStudentByIds(Integer[] ids) {
        return eduStudentMapper.deleteEduStudentByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteEduStudentById(Integer id) {
        return eduStudentMapper.deleteEduStudentById(id);
    }

    /**
     * 根据起始日期获取学生
     */
    @Override
    public List<EduStudent> getStudentByDate(Date d1, Date d2) {
        return eduStudentMapper.getStudentByDate(d1, d2);
    }

    /**
     * 根据起始日期获取学生（贫困学生）
     */
    @Override
    public List<EduStudent> getPoorStudent(Date d1, Date d2) {
        return eduStudentMapper.getPoorStudent(d1, d2);
    }

    /**
     * 根据起始日期获取学生（特殊儿童）
     */
    @Override
    public List<EduStudent> getSpecialStudent(Date d1, Date d2) {
        return eduStudentMapper.getSpecialStudent(d1, d2);
    }

    /**
     * 根据日期和镇id获取学生列表
     */
    @Override
    public List<EduStudent> getStudentByDateAndZone(Date d1, Date d2, Integer zoneId) {
        return eduStudentMapper.getStudentByDateAndZone(d1, d2, zoneId);
    }

    /**
     * 建档立卡户数统计
     */
    @Override
    public Integer statisticalTotal(Date d1, Date d2, Integer zoneId) {
        return eduStudentMapper.statisticalTotal(d1, d2, zoneId);
    }


}
