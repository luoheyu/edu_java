package com.travel.system.service;

import com.travel.system.domain.DdStudentStanding;
import com.travel.system.domain.EduStudent;

import java.util.List;

/**
 * 未入学儿童台账Service接口
 *
 * @author xianyue
 * @date 2021-01-25
 */
public interface IDdStudentStandingService {
    /**
     * 查询未入学儿童台账
     *
     * @param id 未入学儿童台账ID
     * @return 未入学儿童台账
     */
    public DdStudentStanding selectDdStudentStandingById(Integer id);

    /**
     * 查询未入学儿童台账列表
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 未入学儿童台账集合
     */
    public List<DdStudentStanding> selectDdStudentStandingList(DdStudentStanding ddStudentStanding);

    /**
     * 新增未入学儿童台账
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 结果
     */
    public int insertDdStudentStanding(DdStudentStanding ddStudentStanding);

    /**
     * 修改未入学儿童台账
     *
     * @param ddStudentStanding 未入学儿童台账
     * @return 结果
     */
    public int updateDdStudentStanding(DdStudentStanding ddStudentStanding);

    /**
     * 批量删除未入学儿童台账
     *
     * @param ids 需要删除的未入学儿童台账ID
     * @return 结果
     */
    public int deleteDdStudentStandingByIds(Integer[] ids);

    /**
     * 删除未入学儿童台账信息
     *
     * @param id 未入学儿童台账ID
     * @return 结果
     */
    public int deleteDdStudentStandingById(Integer id);
    /**
    *获取统计的年份
    *
    */
    public List<DdStudentStanding> getTotalYear();
    /**
    *批量插入学生信息
    *
    */
    public void insertStudentToStanding(List<EduStudent> list);
}
