package com.travel.system.service.impl;

import com.travel.system.domain.EduSchool;
import com.travel.system.domain.EduZone;
import com.travel.system.mapper.EduSchoolMapper;
import com.travel.system.mapper.EduZoneMapper;
import com.travel.system.service.IEduZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 村镇管理Service业务层处理
 *
 * @author yu
 * @date 2020-12-23
 */

@Service

public class EduZoneServiceImpl implements IEduZoneService {

    @Autowired

    private EduZoneMapper eduZoneMapper;

    @Autowired
    private EduSchoolMapper eduSchoolMapper;


    /**
     * 查询村镇管理
     *
     * @param id 村镇管理ID
     * @return 村镇管理
     */

    @Override

    public EduZone selectEduZoneById(Integer id) {

        return eduZoneMapper.selectEduZoneById(id);

    }


    /**
     * 查询村镇管理列表
     *
     * @param eduZone 村镇管理
     * @return 村镇管理
     */

    @Override

    public List<EduZone> selectEduZoneList(EduZone eduZone) {

        return eduZoneMapper.selectEduZoneList(eduZone);

    }


    /**
     * 新增村镇管理
     *
     * @param eduZone 村镇管理
     * @return 结果
     */

    @Override

    public int insertEduZone(EduZone eduZone) {

        return eduZoneMapper.insertEduZone(eduZone);

    }


    /**
     * 修改村镇管理
     *
     * @param eduZone 村镇管理
     * @return 结果
     */

    @Override

    public int updateEduZone(EduZone eduZone) {

        return eduZoneMapper.updateEduZone(eduZone);

    }


    /**
     * 批量删除村镇管理
     *
     * @param ids 需要删除的村镇管理ID
     * @return 结果
     */

    @Override

    public int deleteEduZoneByIds(Integer[] ids) {

        return eduZoneMapper.deleteEduZoneByIds(ids);

    }


    @Override
    public EduSchool getEsInfo(Integer middleSchoolId) {
        return null;
    }

    /**
     * 删除村镇管理信息
     *
     * @param id 村镇管理ID
     * @return 结果
     */

    @Override

    public int deleteEduZoneById(Integer id) {

        return eduZoneMapper.deleteEduZoneById(id);

    }
    /**
     * 获取所有的镇
     *
     * @param
     * @return 结果
     */
    @Override
    public List<EduZone> getZhen() {
        return eduZoneMapper.getZhen();
    }
    /**
     * 根据id获取村镇名称
     *
     * @param  id
     * @return 结果
     */
    @Override
    public EduZone getNameById(Integer id) {
        return eduZoneMapper.getNameById(id);
    }
    /**
     * 根据fid获取村镇列表
     */
    @Override
    public List<EduZone> getZoneList(Integer fid) {
        return eduZoneMapper.getZoneList(fid);
    }
    /**
     *获取全部村镇
     *
     */
    @Override
    public List<EduZone> getAllZone(){
        return eduZoneMapper.getAllZone();
    };
}
