package com.travel.system.service.impl;

import com.travel.system.domain.EduStudent;
import com.travel.system.domain.TzPoorStudentStanding;
import com.travel.system.mapper.TzPoorStudentStandingMapper;
import com.travel.system.service.ITzPoorStudentStandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 贫困学生台账统计Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-27
 */
@Service
public class TzPoorStudentStandingServiceImpl implements ITzPoorStudentStandingService {
    @Autowired
    private TzPoorStudentStandingMapper tzPoorStudentStandingMapper;

    /**
     * 查询贫困学生台账统计
     *
     * @param id 贫困学生台账统计ID
     * @return 贫困学生台账统计
     */
    @Override
    public TzPoorStudentStanding selectTzPoorStudentStandingById(Integer id) {
        return tzPoorStudentStandingMapper.selectTzPoorStudentStandingById(id);
    }

    /**
     * 查询贫困学生台账统计列表
     *
     * @param tzPoorStudentStanding 贫困学生台账统计
     * @return 贫困学生台账统计
     */
    @Override
    public List<TzPoorStudentStanding> selectTzPoorStudentStandingList(TzPoorStudentStanding tzPoorStudentStanding) {
        return tzPoorStudentStandingMapper.selectTzPoorStudentStandingList(tzPoorStudentStanding);
    }

    /**
     * 新增贫困学生台账统计
     *
     * @param tzPoorStudentStanding 贫困学生台账统计
     * @return 结果
     */
    @Override
    public int insertTzPoorStudentStanding(TzPoorStudentStanding tzPoorStudentStanding) {
        return tzPoorStudentStandingMapper.insertTzPoorStudentStanding(tzPoorStudentStanding);
    }

    /**
     * 修改贫困学生台账统计
     *
     * @param tzPoorStudentStanding 贫困学生台账统计
     * @return 结果
     */
    @Override
    public int updateTzPoorStudentStanding(TzPoorStudentStanding tzPoorStudentStanding) {
        return tzPoorStudentStandingMapper.updateTzPoorStudentStanding(tzPoorStudentStanding);
    }

    /**
     * 批量删除贫困学生台账统计
     *
     * @param ids 需要删除的贫困学生台账统计ID
     * @return 结果
     */
    @Override
    public int deleteTzPoorStudentStandingByIds(Integer[] ids) {
        return tzPoorStudentStandingMapper.deleteTzPoorStudentStandingByIds(ids);
    }

    /**
     * 删除贫困学生台账统计信息
     *
     * @param id 贫困学生台账统计ID
     * @return 结果
     */
    @Override
    public int deleteTzPoorStudentStandingById(Integer id) {
        return tzPoorStudentStandingMapper.deleteTzPoorStudentStandingById(id);
    }
    /**
     *获取统计的年份
     *
     */
    @Override
    public List<TzPoorStudentStanding> getTotalYear(){
        return tzPoorStudentStandingMapper.getTotalYear();
    };
    /**
     *插入贫困学生数据
     *
     */
    @Override
    public void insertStudent(List<EduStudent> list) {
        tzPoorStudentStandingMapper.insertStudent(list);
    }
}
