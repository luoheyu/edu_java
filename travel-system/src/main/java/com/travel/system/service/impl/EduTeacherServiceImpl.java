package com.travel.system.service.impl;

import com.travel.common.utils.DateUtils;
import com.travel.system.domain.EduTeacher;
import com.travel.system.mapper.EduTeacherMapper;
import com.travel.system.service.IEduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师管理Service业务层处理
 *
 * @author xianyue
 * @date 2021-01-06
 */
@Service
public class EduTeacherServiceImpl implements IEduTeacherService {
    @Autowired
    private EduTeacherMapper eduTeacherMapper;

    /**
     * 查询教师管理
     *
     * @param id 教师管理ID
     * @return 教师管理
     */
    @Override
    public EduTeacher selectEduTeacherById(Long id) {
        return eduTeacherMapper.selectEduTeacherById(id);
    }

    /**
     * 查询教师管理列表
     *
     * @param eduTeacher 教师管理
     * @return 教师管理
     */
    @Override
    public List<EduTeacher> selectEduTeacherList(EduTeacher eduTeacher) {
        return eduTeacherMapper.selectEduTeacherList(eduTeacher);
    }

    /**
     * 新增教师管理
     *
     * @param eduTeacher 教师管理
     * @return 结果
     */
    @Override
    public int insertEduTeacher(EduTeacher eduTeacher) {
        eduTeacher.setCreateTime(DateUtils.getNowDate());
        return eduTeacherMapper.insertEduTeacher(eduTeacher);
    }

    /**
     * 修改教师管理
     *
     * @param eduTeacher 教师管理
     * @return 结果
     */
    @Override
    public int updateEduTeacher(EduTeacher eduTeacher) {
        return eduTeacherMapper.updateEduTeacher(eduTeacher);
    }

    /**
     * 批量删除教师管理
     *
     * @param ids 需要删除的教师管理ID
     * @return 结果
     */
    @Override
    public int deleteEduTeacherByIds(Long[] ids) {
        return eduTeacherMapper.deleteEduTeacherByIds(ids);
    }

    /**
     * 删除教师管理信息
     *
     * @param id 教师管理ID
     * @return 结果
     */
    @Override
    public int deleteEduTeacherById(Long id) {
        return eduTeacherMapper.deleteEduTeacherById(id);
    }
}
