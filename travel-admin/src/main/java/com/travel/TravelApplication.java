package com.travel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author xianyue
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class TravelApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(TravelApplication.class, args);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>:ﾞ  系统启动成功");
    }
}
